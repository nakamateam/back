const mongoose = require('mongoose');
const User = mongoose.model('User');

exports.findMyNakamasIds = function(id) {
  return new Promise(function(resolve, reject) {
    User.getFriends(id, {}, {_id:1}, function (err, friends) {
      if (err) {
        reject(err);
      } else {
        resolve(friends);
      }
    });
  });
};

exports.getConnectedUserData = function(usersIds) {
  return new Promise(function(resolve, reject) {
    User.find({'_id': {$in: usersIds}}, {avatarUrl: 1, username: 1, firstName:1, lastName:1, hasAvatar:1}, function(err, users) {
      if (err) {
        reject(err);
      } else {
        users.forEach(function (user) {
          delete user._id;
        });
        console.log(users);
        resolve(users);
      }
    }).lean()
  });
};
