const mongoose = require('mongoose');
const Group = mongoose.model('Group');
const User = mongoose.model('User');


exports.getnakamaWayGroups = function () {
  return new Promise(function (resolve, reject) {
    Group.find({}, {
      'users.notificationPreferences': 0,
      'users.lastConnectionDate': 0,
      'users.userId': 0,
      'users._id': 0,
      creatorId: 0
    }, function (err, groups) {
      if (err) {
        reject(err);
      } else {
        resolve(groups);
      }
    });
  });
};

exports.getNakamaWayTotalUsers = function () {
  return new Promise(function (resolve, reject) {
    User.countDocuments({}, (err, data) => {
      if (err) {
        reject(err)
      }
      resolve(data);
  });
  });
};

exports.getMongoDbStats = function () {
  return new Promise(function (resolve, reject) {
    if (process.env.NODE_ENV.trim() === 'local') {
      mongoose.connect(`mongodb://${process.env.DB_HOST}/${process.env.DB_DATABASE}`, { useNewUrlParser: true })
          .then(()=>{
            console.log('MongoDB is connected to get stats purpose');
            const db = mongoose.connection;
              db.db.stats(function(err, stats) {
                resolve(stats);
              });
          }).catch(err=>{
        console.log(err);
        reject(err);
      });
    } else {
      mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST.trim()}:27017/${process.env.DB_DATABASE}`, { useNewUrlParser: true })
          .then(()=>{
            console.log('MongoDB is connected to get stats purpose');
            const db = mongoose.connection;
            db.db.stats(function(err, stats) {
              resolve(stats);
            });
          }).catch(err=>{
        console.log('MongoDB connection unsuccessful.');
        reject(err);
      });
    }
  });
};


exports.getMyGroupInfo = function (myId, groupId) {
  return new Promise(function (resolve, reject) {
    Group.findOne({'_id': groupId, 'users.userId': myId}, function (err, group) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        let myGroupRole = group.users.find(function (element) {
          return element.userId.toString() === myId;
        });
        let groupCreatorStillPresent = group.users.find(function (element) {
          return element.userId.toString() === group.creatorId.toString();
        });
        group.isAdmin = myGroupRole.groupRole === groupRole.ADMIN.value;
        let arr = group.users.map(ele => new mongoose.Types.ObjectId(ele.userId));
        if (groupCreatorStillPresent === undefined) {
          arr.push(new mongoose.Types.ObjectId(group.creatorId));
        }
        User.find({'_id': {$in: arr}}, {avatarUrl: 1, username: 1, firstName: 1, lastName: 1}, function (err, users) {
          if (err) {
            reject(err);
          } else {
            group.users.forEach(function (groupUser) {
              users.forEach(function (user) {
                if (user._id.toString() === groupUser.userId.toString()) {
                  groupUser.avatarUrl = user.avatarUrl;
                  groupUser.username = user.username;
                  groupUser.firstName = user.firstName;
                  groupUser.lastName = user.lastName;
                  if (groupUser.userId.toString() === group.creatorId.toString()) {
                    groupUser.isCreator = true;
                  }
                }
                if (user._id.toString() === group.creatorId.toString()) {
                  group.creatorDetails = user;
                }
              });
            });
            if (myId.toString() === group.creatorId.toString()) {
              group.isCreator = true;
            }
            delete group.creatorId;
            resolve(group);
          }
        })
      }
    }).lean();
  });
};
