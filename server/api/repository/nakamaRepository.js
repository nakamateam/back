const mongoose = require('mongoose');
const User = mongoose.model('User');
const Notification = mongoose.model('Notification');
const notifEnum = require('../models/notificationEnum');
const notifSocket = require('../../sockets/notifSocket');
const socketIO = require('../../helpers/socketio').get();
const NotificationSocket = new notifSocket(socketIO);

let RequestisAlreadySent = function(myId, newNakamaId, cb) {
  User.getFriends(myId, {_id: newNakamaId},{_id:1}, function (err, friend) {
    if (err) {
      cb(false);
    } else {
      if (friend.length === 0) {
        return cb(false);
      } else {
        if (friend[0].status === 'requested' || friend[0].status === 'accepted') {
          return cb(true);
        } else return cb(false);
      }
    }
  });
};

exports.AutocompletefindNewNakamas = function(id, query) {
  return new Promise(function(resolve, reject) {
    let userQuery = {
      "_id": { "$ne": id },
    "$or": [{"username": {"$regex": query, "$options": "i"}}, {"lastName": {"$regex": query, "$options": "i"}}, {"firstName": {"$regex": query, "$options": "i"}}]
  };
    User.find(userQuery, {username: 1, lastName: 1, firstName: 1, avatarUrl: 1}).limit(6).lean().then(users => {
      User.getFriends(id, function (err, friends) {
        if (err) {
          reject(err);
        } else {
          users.forEach(function (user) {
            friends.forEach(function (friend) {
              if (user._id.toString() === friend._id.toString()) {
                if (friend.status === 'accepted') {
                  //users.splice(users.findIndex(v =>  v._id.toString() === friend._id.toString()), 1);
                  users[(users.findIndex(v => v._id.toString() === friend._id.toString()))].alreadyNakama = true;
                }
                if (friend.status === 'requested') {
                  users[(users.findIndex(v => v._id.toString() === friend._id.toString()))].requestPending = true;
                }
                if (friend.status === 'pending') {
                  users[(users.findIndex(v => v._id.toString() === friend._id.toString()))].requestYou = true;
                }
              }
            });
          });
          resolve(users);
        }
      });
    }).catch(err => {
      reject(err);
    });
  });
};

exports.getMyNakamas = function(id) {
  return new Promise(function(resolve, reject) {
      User.getAcceptedFriends(id, {}, {username: 1, lastName: 1, firstName: 1, avatarUrl: 1}, function (err, friends) {
        console.log(friends);
        if (err) {
          reject(err);
        } else {
          resolve(friends);
        }
      });
  });
};

exports.findMyNakamas = function(id) {
  return new Promise(function(resolve, reject) {
    User.getFriends(id, {}, {username:1, firstName:1, lastName:1, avatarUrl:1}, function (err, friends) {
      if (err) {
        reject(err);
      } else {
        resolve(friends);
      }
    });
  });
};

exports.findMyNakamasIds = function(id) {
  return new Promise(function(resolve, reject) {
    User.getFriends(id, function (err, friends) {
      if (err) {
        reject(err);
      } else {
        resolve(friends);
      }
    });
  });
};


exports.getMyNakamasData = function(nakamas) {
  return new Promise(function(resolve, reject) {
      User.find({'nakamaId': { $in: nakamas}}, { _id:0, firstName:1, lastName: 1, username:1, avatarUrl: 1, nakamaId:1 }
  , function(err, myNakamas){
          if (err) {
            reject(err);
          } else {
            resolve(myNakamas);
          }
      });
  });
};

exports.sendNakamaRequest = function (myId, newNakamaId) {
  console.log(newNakamaId);
  console.log('-------------');
  console.log(myId);
  return new Promise(function (resolve, reject) {
    RequestisAlreadySent(myId, newNakamaId, function (isAlreadyAFriend) {
      if (isAlreadyAFriend) {
        reject('Seems you are already nakama');
      } else {
        User.getFriends(myId, {_id: newNakamaId}, function (err, friend) {
          console.log('NNNNNNOOOOOO CCCCCCCOOOOOOOONNNNNNEEEEEECCCCCCTTTTTTIIIIIIOOOOOONNNNN YYYYYEEEETTTT');
          console.log(friend);
          if (friend.length === 0) {
            User.requestFriend(myId, newNakamaId, function (err, user) {
              if (err) {
                reject(err);
              } else {
                if(user.friend.status === 'pending') {
                  User.findById(myId, {_id: 0, username: 1, avatarUrl: 1}, function(err, user) {
                    if (err) {
                      reject(err);
                    }
                    // Create Notification to let the user know he has a new nakama request
                    let newFriendRequest = new Notification({
                      receiver: newNakamaId,
                      sender: myId,
                      created_at: new Date().toISOString(),
                      notificationType: notifEnum.NAKAMAREQUEST.value
                    });
                    newFriendRequest.save(function(err, notif) {
                      if (err) {
                        reject(err);
                      }
                      NotificationSocket.sendLiveNotification(socketIO, newNakamaId, newFriendRequest);
                      console.log(notif);
                      resolve('Request Sent');
                    });
                  });
                }
              }
            });
          } else {
            reject('Request is no longer valid');
          }
        });
      }
    });
  });
};

exports.acceptNakamaRequest = function (myId, newNakamaId) {
  console.log(newNakamaId);
  console.log('-------------');
  console.log(myId);
  return new Promise(function (resolve, reject) {
    RequestisAlreadySent(myId, newNakamaId, function (isAlreadyAFriend) {
      if (isAlreadyAFriend) {
        reject('Seems you are already nakama');
      } else {
        User.getFriends(myId, {_id: newNakamaId}, function (err, friend) {
          console.log('should be pending');
          console.log(friend);
          if (friend.length === 1) {
            if (friend[0].status === 'pending') {
              User.requestFriend(myId, newNakamaId, function (err, user) {
                if (err) {
                  reject(err);
                } else {
                  console.log('uuuuuuusssssseeeeeeerrrrrr accccept reqqquuuessstttt');
                  console.log(user);
                  if (user.friend.status === 'accepted') {
                    User.findById(myId, {_id:0,username: 1, avatarUrl: 1}, function(err, user) {
                      if (err) {
                        console.log(err);
                        reject(err);
                      }
                      // Create Notification to let the requester know its nakama request has been accepted
                      let newAcceptedNakamadRequest = new Notification({
                        receiver: newNakamaId,
                        sender: myId,
                        created_at: new Date().toISOString(),
                        notificationType: notifEnum.NEWNAKAMACONFIRMATION.value
                      });

                      console.log(newAcceptedNakamadRequest);
                      NotificationSocket.letMyNewNakamaKnowIAmConnected(socketIO, newNakamaId, myId);

                      newAcceptedNakamadRequest.save(function(err, notif) {
                        if (err) {
                          reject(err);
                        }
                        NotificationSocket.sendLiveNotification(socketIO, newNakamaId, newAcceptedNakamadRequest);
                        resolve('Request Accepted');
                      });
                    });
                  }
                }
              });
            } else {
              reject({noLongerValid: true, friendId:newNakamaId});
            }
          } else {
            reject({noLongerValid: true, friendId:newNakamaId});
          }
        });
      }
    });
  });
};

exports.removeNakamaFromNakamas = function(myId, nakamaId){
  return new Promise(function(resolve, reject) {
    const user1Id = mongoose.Types.ObjectId(myId);
    const user2Id = mongoose.Types.ObjectId(nakamaId);
    User.removeFriend(user1Id, user2Id, function (err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.cancelRequestAndUpdateNotification = function(myId, nakamaId){
  return new Promise(function(resolve, reject) {
    const user1Id = mongoose.Types.ObjectId(myId);
    const user2Id = mongoose.Types.ObjectId(nakamaId);
    const deleteQuery = { receiver: nakamaId, sender: myId, notificationType: notifEnum.NAKAMAREQUEST.value  };
    User.removeFriend(user1Id, user2Id, function (err, user) {
      console.log(user);
      if (err) {
        reject(err);
      }
      Notification.findOne(deleteQuery, function (err, lastRequestNotif) {
        console.log('laaaaaastNOOOOTIF');
        console.log(lastRequestNotif);
        if (err) {
          reject(err);
        } else {
          Notification.deleteOne({_id:lastRequestNotif._id}, function (err, notif) {
            console.log(notif);
            if (err) {
              reject(err);
            }
            let canceledRequestNotif = new Notification({
              receiver: nakamaId,
              sender: myId,
              notificationType: notifEnum.REQUESTCANCELED.value
            });
            NotificationSocket.updateLiveNotification(socketIO, nakamaId, canceledRequestNotif);
            resolve(notif);
          });
        }
      }).sort({_id:-1});
    });
  });
};


/** THOSE ARE THE METHOD FOR CUSTOM NAKAMA RELATIONSHIP*/

/** Get users except nakama : /newnakamas */
/*exports.findNewNakamas = function(id) {
  return new Promise(function(resolve, reject) {
    User.findById(id, function(err, me) {
      if (err) {
        reject(err);
      }
      User.find()
        .where('_id').ne(id)
        .select('username firstName lastName avatarUrl nakamaId -_id')
        .lean()
        .exec(function(err, users){
          if (err) {
            reject(err);
          } else {
            me.myNakamas.forEach(function (nakama) {
              console.log(nakama);
              if(nakama.statusType === nakamaStatusEnum.NAKAMA.value){
                users.splice(users.findIndex(v =>  v.nakamaId === nakama.nakamaId), 1);
              }
              if(nakama.statusType === nakamaStatusEnum.NAKAMAREQUESTPENDING.value){
                let test = users.findIndex(v =>  v.nakamaId === nakama.nakamaId);
                console.log(test);
                users[(users.findIndex(v =>  v.nakamaId === nakama.nakamaId))].requestPending = true;
              }
            });
            console.log(users);
            resolve(users);
          }
        })
    });
  });
};*/

/** Get my nakamas and my pending Request : /nakamas */
/*
exports.findMyNakamas = function(id) {
  return new Promise(function(resolve, reject) {
    User.findById(id, function(err, me) {
      if (err) {
        reject(err);
      }
      let myNakamas = [];
      me.myNakamas.forEach(function (nakama) {
        if(nakama.statusType === nakamaStatusEnum.NAKAMA.value){
          myNakamas.push(nakama.nakamaId);
        }
      });
      resolve(myNakamas);
    });
  });
};*/

/** add new nakama and create notification : /nakama  (put) */

/*let checkNakama = function (nakamas, value) {
  return nakamas.some(function (nakama)  {
    return nakama.nakamaId.toString() === value.toString();
  });
};*/

/*exports.findOneAndUpdate = function (newNakamaId, myId) {
  return new Promise(function (resolve, reject) {
    User.findById(myId, function (err, user) {
      if (err) {
        reject(err);
      }
      if (checkNakama(user.myNakamas, newNakamaId)) {
        console.log('loool');
        reject('Seems you are already nakama');
      } else {
        User.findOne({nakamaId: newNakamaId}, function(err, futureNakama) {
          if (err) {
            reject(err);
          }
          // Create Notification for the new nakama
          console.log(futureNakama);
          let newFriendRequest = new Notification({
            receiver: futureNakama._id,
            sender: user._id,
            notificationType: notifEnum.NAKAMAREQUEST.value
          });
          newFriendRequest.save(function(err, notif) {
            if (err) {
              reject(err);
            }
            console.log(notif);
            let newNakamaPending = new Nakama({
              nakamaId: newNakamaId,
              statusType: nakamaStatusEnum.NAKAMAREQUESTPENDING.value
            });
            User.findOneAndUpdate({_id: myId}, {$push: {myNakamas: newNakamaPending}}, {new: true}, function (err, myNakamas) {
              if (err) {
                reject(err);
              }
              resolve(myNakamas);
            });
          });

        });
      }
    });
  });
};*/


/** delete nakama : /nakama  (delete) */
/*exports.removeNakamaFromNakamas = function(myId, nakamaId){
  return new Promise(function(resolve, reject) {
    User.findOneAndUpdate({ _id: myId }, {$pull: {myNakamas: {nakamaId: nakamaId}}}, { new: true }, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};*/
