const mongoose = require('mongoose');
const ChatMessage = mongoose.model('ChatMessage');
const User = mongoose.model('User');


exports.getChatRoomMessages = function(roomId, from, myId) {
  return new Promise(function(resolve, reject) {
    ChatMessage.find({'groupId': roomId}, {groupId: 1, file:1, userAvatar:1, username: 1, message:1, postedAt:1, timestamp:1, linkMetaData:1}, function(err, messages) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve(messages)
      }
    }).sort({_id:-1}).skip(Number(from)).limit(30);
  });
};

exports.getChatLinks = function(roomId) {
  return new Promise(function(resolve, reject) {
    ChatMessage.find({'groupId': roomId, linkMetaData: { $exists: true }, 'linkMetaData.ogUrl': { $exists: true }}, {linkMetaData:1, userAvatar:1, username: 1, postedAt:1}, function(err, messages) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve(messages)
      }
    }).sort({_id:1});
  });
};

exports.getUnreadMesage = function(roomId, myId) {
  return new Promise(function(resolve, reject) {
    ChatMessage.find({'groupId': roomId, seenBy: { $elemMatch: { userId: myId, hasBeenRead: false }}}, function(err, messages) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve(messages.length)
      }
    });
  });
};

exports.updateUnreadMessagesInRoom = function(roomId, myId) {
  return new Promise(function(resolve, reject) {
    ChatMessage.update({'groupId': roomId, seenBy: { $elemMatch: { userId: myId, hasBeenRead: false }}}, { $set: { 'seenBy.$.hasBeenRead' : true,  'seenBy.$.seenAt': new Date()}}, { multi: true }, function(err, messages) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve({updated: true})
      }
    });
  });
};

exports.findById = function(id) {
  return new Promise(function(resolve, reject) {
    User.findById(id, {username: 1, avatarUrl: 1, firstName:1, lastName: 1, hasAvatar:1}, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};
