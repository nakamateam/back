const mongoose = require('mongoose');
const newUserGroup = require('../models/userGroupModel');
const Group = mongoose.model('Group');
const User = mongoose.model('User');
const File = mongoose.model('File');
const Notification = mongoose.model('Notification');
const ChatMessage = mongoose.model('ChatMessage');
const notifPrefEnum = require('../models/notifPreferenceEnum');
const multer = require('multer');
const fs = require('fs');
const groupRole = require('../models/groupRoleEnum');
const notifEnum = require('../models/notificationEnum');
const notifSocket = require('../../sockets/notifSocket');
const socketIO = require('../../helpers/socketio').get();
const path = require('path');
const filePath = path.dirname(require.main.filename);
const NotificationSocket = new notifSocket(socketIO);
const wayAvatarStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/way/avatar')
  },
  filename: function (req, file, cb) {
    const ext = file.mimetype.split('/')[1];
    cb(null, file.fieldname + '_' + Date.now() + '.' + ext);
  }
});
const limits = { fileSize: 10 * 1024 * 1024 };
const uploadGroupAvatar = multer({
  storage: wayAvatarStorage,
  limits: limits, //10Mb
}).single('way-avatar');
const imagemin = require('imagemin');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

/** Verify if new group partipants are 'accepted' nakamas */
let areThoseParticipantsMyNakamas = function (group, myId) {
  return new Promise(function (resolve, reject) {
    let participantLength = group.users.length;
    User.getAcceptedFriends(myId, {}, {_id: 1}, function (err, user) {
      if (err) {
        reject(err);
      }
      console.log(user);
      if (user.length > 0) {
        let myNakamasIds = user.map(friend => friend._id.toString());
        group.users.forEach(function (participant, i) {
          if (myNakamasIds.includes(participant.userId.toString())) {
            if (i === participantLength - 1) {
              resolve()
            }
          } else {
            reject('Seems you do not have any nakamas yet');
          }
        });
      } else {
        reject('seems you try to create a group without a nakama')
      }
    });
  })
};

/** admin right checker function */
let areYouAWayAdmin = function (myId, wayId, res) {
  return new Promise(function (resolve, reject) {
    Group.findById(wayId, function (err, group) {
      if (err) {
        reject(err);
      }
      let myGroupRole = group.users.find(function (element) {
        return element.userId.toString() === myId.toString();
      });
      if (!myGroupRole) {
        res.status(405).send('It seems you do not belong to the group');
      } else if (myGroupRole.groupRole === groupRole.PARTICIPANT.value) {
        res.status(406).send('It seems you do not have the right permission to perform this operation');
      } else if (myGroupRole.groupRole === groupRole.ADMIN.value) {
        resolve(group);
      }
    });
  })
};

exports.AutocompletefindAGroup = function(id, query) {
  return new Promise(function(resolve, reject) {
        let groupQuery = {
          'users.userId': id,
          "$and": [{groupName: {"$regex": query, "$options": "i"}}]
        };
        Group.find(groupQuery, {groupName: 1, users: 1, avatarUrl: 1}).limit(6)
          .then(groups => {
            console.log(groups);
            resolve(groups);
          })
          .catch(err => {
            reject(err);
          });
  });
};

exports.save = function (group, myId) {
  return new Promise(function (resolve, reject) {
    return areThoseParticipantsMyNakamas(group, myId)
      .then(function () {
        group.save(function (err, group) {
          if (err) {
            reject(err);
          }
          console.log(group);
          group.users.forEach(function (user, i) {
            console.log('user');
            console.log(user);
            User.findById(user.userId,{avatarUrl:1, username:1}, function (err, user) {
              if (err) {
                reject(err);
                console.log(err);
              } else {
                console.log('user._id is type : ' + typeof user._id + 'and is equal to : ' + user._id);
                console.log('group.creatorId is type : ' + typeof group.creatorId + 'and is equal to : ' + group.creatorId);
                if (user._id.toString() !== group.creatorId.toString()) {
                  // Create Notification to let user know he has been invited to a new group
                  console.log(group.groupName);
                  let newGroupInvitation = new Notification({
                    receiver: user._id,
                    sender: myId,
                    created_at: new Date().toISOString(),
                    notificationType: notifEnum.NEWGROUPINVITATION.value,
                    metaData: {groupId:group._id}
                  });
                  newGroupInvitation.save(function (err, notif) {
                    if (err) {
                      reject(err);
                      console.log(err);
                    }
                    notif.senderInfo = user;
                    notif.groupInfo = {groupName: group.groupName, avatarUrl: group.avatarUrl};
                    NotificationSocket.sendLiveNotification(socketIO, user._id.toString(), notif);
                    console.log(notif);
                  });
                }
                if (i === group.users.length - 1) {
                  resolve(group);
                }
              }
            });
          });
        });
      }, function (error) {
        reject(error);
      });
  });
};


exports.getMyGroups = function(myId) {
  return new Promise(function(resolve, reject) {
      Group.find({'users.userId': myId}, {hasAvatar: 0, creatorId: 0}, function(err, groups) {
          if (err) {
            console.log(err);
            reject(err);
          } else {
            groups.forEach(function (group) {
              let me = group.users.find(function(element) {
                return element.userId.toString() === myId;
              });
              group.isAdmin = me.groupRole === groupRole.ADMIN.value;
              group.lastConnectionDate = me.lastConnectionDate;
            });
            console.log('here is all my groups');
            console.log(groups);
            resolve(groups);
          }
        }).lean();
  });
};

exports.getMyGroupRole = function(myId, groupId) {
  return new Promise(function(resolve, reject) {
        Group.findOne({'_id': groupId, 'users.userId': myId}, {hasAvatar: 0, creatorId: 0, creationDate: 0}, function(err, group) {
          if (err) {
            console.log(err);
            reject(err);
          } else {
              let myGroupRole = group.users.find(function(element) {
                return element.userId.toString() === myId;
              });
              group.isAdmin = myGroupRole.groupRole === groupRole.ADMIN.value;
              delete group.users;
            console.log(group);
            resolve(group);
          }
        }).lean();
  });
};


exports.getMyGroupInfo = function(myId, groupId) {
  return new Promise(function(resolve, reject) {
    Group.findOne({'_id': groupId, 'users.userId': myId}, function(err, group) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        let myGroupRole = group.users.find(function(element) {
          return element.userId.toString() === myId;
        });
        let groupCreatorStillPresent = group.users.find(function(element) {
          return element.userId.toString() === group.creatorId.toString();
        });
        group.isAdmin = myGroupRole.groupRole === groupRole.ADMIN.value;
        let arr = group.users.map(ele => new mongoose.Types.ObjectId(ele.userId));
        if (groupCreatorStillPresent === undefined) {
          arr.push(new mongoose.Types.ObjectId(group.creatorId));
        }
        User.find({'_id': {$in: arr}}, {avatarUrl: 1, username: 1, firstName:1, lastName:1}, function(err, users) {
          if (err) {
            reject(err);
          } else {
            group.users.forEach(function (groupUser) {
              users.forEach(function (user) {
                if (user._id.toString() === groupUser.userId.toString()){
                  console.log('in ze bokle');
                  groupUser.avatarUrl = user.avatarUrl;
                  groupUser.username = user.username;
                  groupUser.firstName = user.firstName;
                  groupUser.lastName = user.lastName;
                  if (groupUser.userId.toString() === group.creatorId.toString()){
                    groupUser.isCreator = true;
                  }
                }
                if (user._id.toString() === group.creatorId.toString()){
                  group.creatorDetails = user;
                }
                  });
            });
            if (myId.toString() === group.creatorId.toString()){
              group.isCreator = true;
            }
            console.log('is ze creator still sentpre?');
            console.log(groupCreatorStillPresent);
            delete group.creatorId;
            console.log(users);
            resolve(group);
          }
        })
      }
    }).lean();
  });
};

exports.getMyGroupPreferences = function(myId, groupId) {
  return new Promise(function(resolve, reject) {
    Group.findOne({'_id': groupId, 'users.userId': myId}, {users: 1}, function(err, group) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        let myPreferences = group.users.find(function(element) {
          return element.userId.toString() === myId;
        });
        if (myPreferences === undefined) {
          reject(err);
        } else {
          let notificationsPreferences = myPreferences.notificationPreferences;
          resolve(notificationsPreferences);
        }
      }
    }).lean();
  });
};

exports.searchNewMembersAutocomplete = function(id, query, wayId) {
  return new Promise(function(resolve, reject) {
    let userQuery = {
      "_id": { "$ne": id },
      "$or": [{"username": {"$regex": query, "$options": "i"}}, {"lastName": {"$regex": query, "$options": "i"}}, {"firstName": {"$regex": query, "$options": "i"}}]
    };
    Group.findById(wayId, function (err, group) {
      if (err) {
        reject(err);
      }
      User.getFriends(id, userQuery, {avatarUrl: 1, firstName:1, lastName:1, username: 1}, function (err, friends) {
        if (err) {
          reject(err);
        } else {
          console.log(friends);
            friends.forEach(function (friend) {
              delete friend.added;
              delete friend.status;
              group.users.forEach(function (member) {
                if (member.userId.toString() === friend._id.toString()) {
                  friend.isAlreadyMember = true;
              } else if (member.userId.toString() !== friend._id.toString() && (friend.isAlreadyMember && friend.isAlreadyMember !== true) ) {
                friend.isAlreadyMember = false;
              }
            });
          });
          resolve(friends);
        }
      });
    });
  });
};

exports.saveAvatar = function (req, res) {
  return new Promise(function (resolve, reject) {
    const wayId = req.headers['way-id'];
    console.log('---------------------RAAAAAAAYYYYYYY IIIID------------------');
    console.log(wayId);
    if (wayId) {
      return areYouAWayAdmin(req.user._id, wayId, res)
        .then(function (group) {
          uploadGroupAvatar(req, res, function (err) {
              if (err) {
                console.log(err);
                reject(err);
              } else {
                console.log('----------------req.file-------------');
                console.log(req.file);
                if (group.hasAvatar) {
                  console.log('Jai un group avatar');
                  module.exports.removeGroupAvatarById(req.body.wayId)
                    .then(function (removedAvatar) {
                      module.exports.createAvatarAndUpdateGroup(req)
                        .then(function (avatarUrl) {
                          res.status(200).end(avatarUrl);
                        }, function (error) {
                          res.status(400).send(error);
                        })
                    }, function (error) {
                      console.log(error);
                      res.status(400).send(error);
                    });
                } else {
                  console.log('Je nai pas davatar');
                  module.exports.createAvatarAndUpdateGroup(req)
                    .then(function (avatarUrl) {
                      res.status(200).end(avatarUrl);
                    }, function (error) {
                      res.status(400).send(error);
                    })
                }
              }
            }
          );
        }, function (error) {
          reject(error);
        });
    } else {
      reject('something went wrong with the group header');
    }
  });
};

function handleNewGroupAvatarNotification(group, req, avatar) {
  group.users.forEach(function (user) {
    if (user.userId.toString() !== req.user._id.toString() && user.notificationPreferences.settings) {
      User.findById(user.userId, {avatarUrl:1, username:1}, function (err, user) {
        if (err) {
          reject(err);
          console.log(err);
        } else {
          let newGroupAvatar = new Notification({
            receiver: user._id,
            sender: req.user._id,
            created_at: new Date().toISOString(),
            notificationType: notifEnum.NEWGROUPAVATAR.value,
            metaData: {groupId : group._id, fileId: avatar._id}
          });
          newGroupAvatar.save(function (err, notif) {
            if (err) {
              reject(err);
            }
            notif.senderInfo = user;
            notif.groupInfo = {groupName: group.groupName, avatarUrl: group.avatarUrl};
            NotificationSocket.sendLiveNotification(socketIO, user._id.toString(), notif);
          });
        }
      });
    }
  });
}

exports.createAvatarAndUpdateGroup = function (req) {
  return new Promise(function (resolve, reject) {
    console.log('-------------------req.file.agaiiiiiiiin-------------------');
    console.log(req.file);
    const newAvatar = new File(req.file);
    newAvatar.postedBy = req.user._id;
    newAvatar.groupId = req.body.wayId;
    newAvatar.save(function (err, avatar) {
      if (err) {
        console.log(err);
        reject(err);
      }
      console.log(avatar);
      (async () => {
        await imagemin([avatar.path], 'public/way/avatar', {
          use: [
            imageminPngquant(),
            imageminMozjpeg(imageminMozjpeg({
              quality: 10
            })),
          ]
        });
        console.log('avatar optimized');
      })();
      const avatarUrl = process.env.SERVER_URL + avatar.path;
      Group.findOneAndUpdate({_id: req.body.wayId}, {
        $set: {
          hasAvatar: true,
          avatarUrl: avatarUrl
        }
      }, {new: true}, function (err, group) {
        if (err) {
          reject(err);
        }
        /** create Notification following user preferences if at least one user in the group has subscribed to settings */
        if (group.users.filter(user => user.notificationPreferences.settings).length > 0) {
          handleNewGroupAvatarNotification(group, req, avatar);
        }
        resolve(group.avatarUrl);
      });
    })
  });
};

exports.removeGroupAvatarById = function (groupId) {
  return new Promise(function (resolve, reject) {
    let path;
    File.findOne({fieldname: 'way-avatar', groupId: groupId}, function (err, avatarFile) {
      if (err) {
        reject(err);
      }
      path = avatarFile.path;
      File.remove({_id: avatarFile._id}, function (err, removedAvatar) {
        if (err) {
          reject(err);
        }
        console.log(filePath + '/' + path);
        fs.unlink(filePath + '/' + path, (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          } else {
            resolve(removedAvatar);
          }
        });
      });
    });
  });
};

exports.resetAvatarByGroupId = function (myId, wayId, res) {
  return new Promise(function (resolve, reject) {
    return areYouAWayAdmin(myId, wayId, res)
      .then(function () {
        let groupAvatarFilePath;
        File.findOne({fieldname: 'way-avatar', groupId: wayId}, function (err, groupAvatar) {
          if (err) {
            reject(err);
          }
          groupAvatarFilePath = groupAvatar.path;
          File.remove({_id: groupAvatar._id}, function (err, avatar) {
            if (err) {
              reject(err);
            }
            fs.unlink(filePath + '/' + groupAvatarFilePath, (err) => {
              if (err) {
                console.log("failed to delete local image:" + err);
              } else {
                Group.findOneAndUpdate({_id: wayId}, {
                  $set: {
                    hasAvatar: false,
                    avatarUrl: process.env.DEFAULT_AVATAR_URL
                  }
                }, {new: true}, function (err, group) {
                  if (err) {
                    reject(err);
                  }
                  resolve(group.avatarUrl);
                });
              }
            });
          });
        });
      }, function (error) {
        reject(error);
      });
  });
};

function handleNewGroupNameNotification(group, myId, wayId, oldName) {
  console.log('_______________wayId is:' + typeof wayId);
  group.users.forEach(function (user) {
    if (user.userId.toString() !== myId && user.notificationPreferences.settings) {
      User.findById(user.userId, {avatarUrl:1, username:1}, function (err, user) {
        if (err) {
          reject(err);
          console.log(err);
        } else {
          let newGroupName = new Notification({
            receiver: user._id,
            sender: myId,
            created_at: new Date().toISOString(),
            notificationType: notifEnum.NEWGROUPNAME.value,
            metaData: {groupId : wayId, oldGroupName:oldName, newGroupName: group.groupName}
          });
          newGroupName.save(function (err, notif) {
            if (err) {
              reject(err);
            }
            notif.senderInfo = user;
            notif.groupInfo = {groupName: group.groupName, avatarUrl: group.avatarUrl};
            NotificationSocket.sendLiveNotification(socketIO, user._id.toString(), notif);
          });
        }
      });
    }
  });
}

exports.saveGroupName = function(myId, newGroupName, wayId, res){
  return new Promise(function(resolve, reject) {
    return areYouAWayAdmin(myId, wayId, res)
      .then(function (oldGroup) {
        Group.findOneAndUpdate({ _id: wayId }, {$set:{groupName:newGroupName}}, { new: true }, function(err, group) {
          if (err) {
            reject(err);
          }
          /** create Notification following user preferences if at least one user in the group has subscribed to settings */
          if (group.users.filter(user => user.notificationPreferences.settings).length > 0) {
            handleNewGroupNameNotification(group, myId, wayId, oldGroup.groupName);
          }
          resolve(group.groupName);
        });
      }, function (error) {
        reject(error);
      });
  });
};

exports.updateGroupNotifPreferences = function(myId, wayId, notifPrefType, newPrefValue, res){
  return new Promise(function(resolve, reject) {
    if (notifPrefType === notifPrefEnum.ALLNOTIFOFF.value) {
        let updatePref={};
        updatePref["users.$.notificationPreferences."+notifPrefEnum.ALLNOTIFOFF.value]=newPrefValue;
        updatePref["users.$.notificationPreferences."+notifPrefEnum.CHAT.value]=!newPrefValue;
        updatePref["users.$.notificationPreferences."+notifPrefEnum.CALENDAR.value]=!newPrefValue;
        updatePref["users.$.notificationPreferences."+notifPrefEnum.SHAREDFILES.value]=!newPrefValue;
        updatePref["users.$.notificationPreferences."+notifPrefEnum.SETTINGS.value]=!newPrefValue;
      console.log(updatePref);
      Group.findOneAndUpdate({
        _id: wayId, "users.userId": myId
      }, {$set: updatePref}, {new: true}, function (err, group) {
        if (err) {
          reject(err);
        }
        let myPreferences = group.users.find(function(element) {
          return element.userId.toString() === myId;
        });
        if (myPreferences === undefined) {
          reject(err);
        } else {
          let notificationsPreferences = myPreferences.notificationPreferences;
          resolve(notificationsPreferences);
        }
      }).lean();
    } else if (notifPrefType === notifPrefEnum.CHAT.value || notifPrefType === notifPrefEnum.CALENDAR.value || notifPrefType === notifPrefEnum.SHAREDFILES.value || notifPrefType === notifPrefEnum.SETTINGS.value){
      let updatePref={};
      updatePref["users.$.notificationPreferences."+notifPrefType]=newPrefValue;
      console.log(updatePref);
      Group.findOneAndUpdate({
        _id: wayId, "users.userId": myId
      }, {$set: updatePref}, {new: true}, function (err, group) {
        if (err) {
          reject(err);
        }
        let myPreferences = group.users.find(function(element) {
          return element.userId.toString() === myId;
        });
        if (myPreferences === undefined) {
          reject(err);
        } else {
          let notificationsPreferences = myPreferences.notificationPreferences;
          resolve(notificationsPreferences);
        }
      }).lean();
    } else {
      res.status(405).send('It seems your request is invalid');
    }
  });
};

exports.makeParticipant = function(myId, userId, wayId, res){
  return new Promise(function(resolve, reject) {
    return areYouAWayAdmin(myId, wayId, res)
      .then(function () {
        Group.findOneAndUpdate({
          _id: wayId,
          "users._id": userId
        }, {$set: {"users.$.groupRole": groupRole.PARTICIPANT.value}}, {new: true}, function (err, group) {
          if (err) {
            reject(err);
          }
          resolve({groupRole: groupRole.PARTICIPANT.value});
        });
    }, function (error) {
        reject(error);
      });
  });
};

exports.addNewMember = function(myId, userId, wayId, res){
  return new Promise(function(resolve, reject) {
    return areYouAWayAdmin(myId, wayId, res)
      .then(function (group) {
        const isAlreadyPartOfTheGroup = group.users.find(function (element) {
          return element.userId.toString() === userId.toString();
        });
        if (isAlreadyPartOfTheGroup) {
          res.status(406).send('It seems this user is already a member of ' + group.groupName);
        } else {
          const newGroupUser = new newUserGroup({
            userId: userId,
            groupRole: groupRole.PARTICIPANT.value,
            joinDate: Date.now(),
            });
          Group.findOneAndUpdate({_id: wayId}, {$push: {users: newGroupUser}}, {new: true}, function (err, updatedGroup) {
            if (err) {
              reject(err);
            }
            const newUser = updatedGroup.users.find(function (element) {
              return element.userId.toString() === userId.toString();
            });
            User.findById(userId, {avatarUrl: 1, username: 1, firstName:1, lastName:1}, function(err, user) {
              if (err) {
                reject(err);
              }
              newUser.avatarUrl = user.avatarUrl;
              newUser.username = user.username;
              newUser.firstName = user.firstName;
              newUser.lastName = user.lastName;

              /** Create Notification to let user know he has been invited to a new group */
              User.findById(myId, {avatarUrl:1, username:1}, function (err, user) {
                if (err) {
                  reject(err);
                } else {
                    let newGroupInvitation = new Notification({
                      receiver: userId,
                      sender: myId,
                      created_at: new Date().toISOString(),
                      notificationType: notifEnum.NEWGROUPINVITATION.value,
                      metaData: {groupId: group._id}
                    });
                    newGroupInvitation.save(function (err, notif) {
                      if (err) {
                        reject(err);
                        console.log(err);
                      }
                      notif.senderInfo = user;
                      notif.groupInfo = {groupName: group.groupName, avatarUrl: group.avatarUrl};
                      NotificationSocket.sendLiveNotification(socketIO, userId.toString(), notif);
                    });
                }
              });
              resolve(newUser);
            });
          }).lean();
        }
      }, function (error) {
        reject(error);
      });
  });
};

exports.makeAdmin = function(myId, userId, wayId, res){
  return new Promise(function(resolve, reject) {
    return areYouAWayAdmin(myId, wayId, res)
      .then(function () {
        Group.findOneAndUpdate({ _id: wayId, "users._id": userId }, {$set:{"users.$.groupRole" :groupRole.ADMIN.value}}, { new: true }, function(err, group) {
          if (err) {
            reject(err);
          }
          resolve({groupRole: groupRole.ADMIN.value});
        });
      }, function (error) {
        reject(error);
      });
  });
};

exports.removeGroupUser = function(myId, userId, wayId, res){
  return new Promise(function(resolve, reject) {
    return areYouAWayAdmin(myId, wayId, res)
      .then(function () {
        Group.findOneAndUpdate({ _id: wayId }, {$pull: {users: {_id: userId}}}, { new: true }, function(err, group) {
          if (err) {
            reject(err);
          }
          resolve({userRemovedId: userId});
        });
      }, function (error) {
        reject(error);
      });
  });
};

exports.updateLastGroupConnection = function(myId, wayId, newDate){
  return new Promise(function(resolve, reject) {
    Group.findOneAndUpdate({_id: wayId, "users.userId": myId}, {$set: {"users.$.lastConnectionDate": newDate}}, {new: true}, function (err, group) {
      if (err) {
        reject(err);
      }
      resolve(newDate);
    });

  });
};

function deleteAllGroupRelatedNotification(groupId) {
  Notification.deleteMany({'metaData.groupId':groupId}, function (err, deletedNotif) {
    if (err) {
      console.log(err)
    } else {
      console.log('___________________________deletion succeeded___________________________');
    }
  });
}

function deleteChatRelatedNotification(groupId) {
  ChatMessage.deleteMany({groupId:groupId}, function (err, deletedChatConv) {
    if (err) {
      console.log(err)
    } else {
      console.log('___________________________deletedChatConv___________________________');
    }
  });
}


function deleteGroupRelatedNotificationByUser(groupId, userId) {
  Notification.deleteMany({'metaData.groupId':groupId, receiver: userId}, function (err, deletedNotif) {
    if (err) {
      console.log(err)
    } else {
      console.log('___________________________deletion succeeded___________________________');
    }
  });
}

exports.removeGroup = function(myId, groupId){
  return new Promise(function(resolve, reject) {
    Group.findById(groupId, {users: 1}, function (err, group) {
      if (err) {
        reject(err);
      } else {
        let me = group.users.find(function(user) {
          return user.userId.toString() === myId;
        });
        console.log(group);
        /** I am the only member remaining on the group */
        if (group.users.length === 1 && group.users[0].userId.toString() === myId) {
          /** delete the group */
          console.log('i am the only one left on this group');
                const deleteQuery = { _id: groupId };
                  Group.deleteOne(deleteQuery, function (err, group) {
                    console.log(group);
                    if (err) {
                      reject(err);
                    }
                    deleteAllGroupRelatedNotification(groupId);
                    deleteChatRelatedNotification(groupId);
                    resolve(group);
                  });
        }
        /** Am I Admin and if so, am I the only admin */
        else if (group.users.length > 1) {
          const adminNumber = group.users.filter(user => user.groupRole === groupRole.ADMIN.value);
          /** As I am the only admin, give to all other members ADMIN privileges before leaving the group */
          if (me && me.groupRole === groupRole.ADMIN.value && adminNumber.length === 1) {
            console.log('I am the only admin in this group');
            /** remove myself from the group 'users' */
                Group.findOneAndUpdate({ _id: groupId }, {$pull: {users: {userId: myId}}}, { new: true }, function(err, group) {
                  if (err) {
                    reject(err);
                  }
                  deleteGroupRelatedNotificationByUser(groupId, myId);
                  console.log(group);
                    /** Then make all remaining users ADMIN and forEach of them update their privileges into 'myGroups' */
                    console.log(me);
                    Group.findOneAndUpdate({ _id: groupId }, {$set:{'users.$[].groupRole':groupRole.ADMIN.value}}, { new: true }, function(err, group) {
                      if (err) {
                        reject(err);
                      }
                      console.log(group);
                      resolve(group);
                    });
                });
          }
          /** I am not the only Admin or I am participant in this group */
          else if ((me && me.groupRole === groupRole.ADMIN.value && adminNumber.length > 1) || (me && me.groupRole === groupRole.PARTICIPANT.value)) {
            console.log('We are ' + adminNumber.length + ' admins in this group');
            /** remove myself from the group 'users' */
            Group.findOneAndUpdate({ _id: groupId }, {$pull: {users: {userId: myId}}}, { new: true }, function(err, group) {
              if (err) {
                reject(err);
              }
              deleteGroupRelatedNotificationByUser(groupId, myId);
              console.log(group);
              resolve(group);
            });
          }
        }
      }
    })
  });
};


/** THOSE ARE group methods used when saving group inside User AND inside Group*/
/*
exports.save = function (group, myId) {
  return new Promise(function (resolve, reject) {
    return areThoseParticipantsMyNakamas(group, myId)
      .then(function () {
        group.save(function (err, group) {
          if (err) {
            reject(err);
          }
          console.log(group);
          group.users.forEach(function (user, i) {
            console.log('user');
            console.log(user);
            const newGroupData = new groupData({groupId: group._id, groupRole: user.groupRole});
            console.log('newGroupData');
            console.log(newGroupData);
            User.findOneAndUpdate({_id: user.userId}, {$push: {myGroups: newGroupData}}, {
              fields: {
                username: 1,
                avatarUrl: 1
              }, new: true
            }, function (err, user) {
              if (err) {
                reject(err);
                console.log(err);
              } else {
                console.log('user._id is type : ' + typeof user._id + 'and is equal to : ' + user._id);
                console.log('group.creatorId is type : ' + typeof group.creatorId + 'and is equal to : ' + group.creatorId);
                if (user._id.toString() !== group.creatorId.toString()) {
                  /!** Create Notification to let user know he has been invited to a new group *!/
                  let newGroupInvitation = new Notification({
                    receiver: user._id,
                    sender: myId,
                    senderInfo: user,
                    created_at: new Date().toISOString(),
                    notificationType: notifEnum.NEWGROUPINVITATION.value,
                    metaData: group.groupName
                  });
                  newGroupInvitation.save(function (err, notif) {
                    if (err) {
                      reject(err);
                    }
                    NotificationSocket.sendLiveNotification(socketIO, user._id.toString(), newGroupInvitation);
                    console.log(notif);
                  });
                }
                if (i === group.users.length - 1) {
                  resolve(group);
                }
              }
            });
          });
        });
      }, function (error) {
        reject(error);
      });
  });
};

exports.getMyGroups = function(myId) {
  return new Promise(function(resolve, reject) {
    User.findById(myId, {_id:0, myGroups: 1}, function(err, myGroups) {
      if (err) {
        reject(err);
      }
      let arr = myGroups.myGroups.map(ele => new mongoose.Types.ObjectId(ele.groupId));
      console.log(arr);
      console.log('my Groups are those: ');
      console.log(myGroups);
      Group.find({'_id': { $in: arr},'users.userId': myId}, {hasAvatar: 0, creatorId: 0}, function(err, groups) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          groups.forEach(function (group) {
            let me = group.users.find(function(element) {
              return element.userId.toString() === myId;
            });
            group.isAdmin = me.groupRole === groupRole.ADMIN.value;
          });
          console.log('here is all my groups');
          console.log(groups);
          resolve(groups);
        }
      }).lean();
    });
  });
};

exports.getMyGroup = function(myId, groupId) {
  return new Promise(function(resolve, reject) {
    User.findOne({'_id':myId, 'myGroups.groupId':groupId}, {_id:0, myGroups: 1}, function(err, me) {
      if (err) {
        reject(err);
      }
      console.log('mmmmmmmmmEEEEEEEEEE');
      console.log(me);
      if (me === undefined || me === null) {
        reject('Are you sure this group exists and if so to be part of it');
      } else {
        Group.findOne({'_id': groupId, 'users.userId': myId}, {hasAvatar: 0, creatorId: 0}, function(err, group) {
          if (err) {
            console.log(err);
            reject(err);
          } else {
              let myGroupRoleInsideMe = me.myGroups.find(function(element) {
                return element.groupId.toString() === groupId;
              });
              group.isAdmin = myGroupRoleInsideMe.groupRole === groupRole.ADMIN.value;
            //let myGroupRoleInsideGroup = group.users.find(function(element) {
              //return element.userId.toString() === myId;
            //});
            //if (myGroupRoleInsideMe.groupRole === groupRole.ADMIN.value && myGroupRoleInsideGroup.groupRole === groupRole.ADMIN.value) {
              //group.isAdmin = true;
            //}
            //if (myGroupRoleInsideMe.groupRole === groupRole.PARTICIPANT.value && myGroupRoleInsideGroup.groupRole === groupRole.PARTICIPANT.value) {
              //group.isAdmin = false;
            //}
              console.log('Zeeeeeeeeeeeee Grouuuuuuuuuuuuuuuuup');
              console.log(group);
              resolve(group);
            }
          }).lean();
        }
      });
    });
};

exports.AutocompletefindAGroup = function(id, query) {
  return new Promise(function(resolve, reject) {
    User.findById(id, {_id: 0, myGroups: 1}, function (err, myGroups) {
      if (err) {
        reject(err);
      }
      else {
        let arr = myGroups.myGroups.map(ele => new mongoose.Types.ObjectId(ele.groupId));
        let groupQuery = {
          'users.userId': id,
          "$and": [{groupName: {"$regex": query, "$options": "i"}}, {'_id': {$in: arr}}]
        };
        Group.find(groupQuery, {groupName: 1, users: 1, avatarUrl: 1}).limit(6)
          .then(groups => {
            console.log(groups);
            resolve(groups);
          })
          .catch(err => {
            reject(err);
          });
      }
    });
  });
};

exports.removeGroup = function(myId, groupId){
  return new Promise(function(resolve, reject) {
    Group.findById(groupId, {users: 1}, function (err, group) {
      if (err) {
        reject(err);
      } else {
        let me = group.users.find(function(user) {
          return user.userId.toString() === myId;
        });
        console.log(group);
        // I am the only member remaining on the group
if (group.users.length === 1 && group.users[0].userId.toString() === myId) {
  // First remove the group from 'myGroups' and then delete the group
  console.log('i am the only one left on this group');
  User.findOneAndUpdate({ _id: myId }, {$pull: {myGroups: {groupId: groupId}}}, { new: true }, function(err, me) {
    if (err) {
      reject(err);
    }
    // Then delete the group
    console.log(me);

    const deleteQuery = { _id: groupId };
    Group.deleteOne(deleteQuery, function (err, group) {
      console.log(group);
      if (err) {
        reject(err);
      }
      resolve(group);
    });
  });
}
// Am I Admin and if so, am I the only one
else if (group.users.length > 1) {
  const adminNumber = group.users.filter(user => user.groupRole === groupRole.ADMIN.value);
  // As I am the only admin, give to all other members ADMIN privileges before leaving the group
  if (me && me.groupRole === groupRole.ADMIN.value && adminNumber.length === 1) {
    console.log('I am the only admin in this group');
    // first remove myself from the group 'users' and then from my groups
    Group.findOneAndUpdate({ _id: groupId }, {$pull: {users: {userId: myId}}}, { new: true }, function(err, group) {
      if (err) {
        reject(err);
      }
      console.log(group);
      User.findOneAndUpdate({ _id: myId }, {$pull: {myGroups: {groupId: groupId}}}, { new: true }, function(err, me) {
        if (err) {
          reject(err);
        }
        // Then make all remaining users ADMIN and forEach of them update their privileges into 'myGroups'
        console.log(me);
        Group.findOneAndUpdate({ _id: groupId }, {$set:{'users.$[].groupRole':groupRole.ADMIN.value}}, { new: true }, function(err, group) {
          if (err) {
            reject(err);
          }
          // Then make all remaining users ADMIN and forEach of them update their privileges into 'myGroups'
          // let arr = group.users.map(ele => new mongoose.Types.ObjectId(ele.userId));
          console.log(group);
          User.update({ 'myGroups.groupId': groupId}, { $set: { 'myGroups.$.groupRole' : groupRole.ADMIN.value } },{ multi: true }, function(err, groupMembers){
            if (err) {
              console.log(err);
              reject(err);
            } else {
              console.log('GGGGGRRRRRRROUUUUUPPPPP MMMEEEEEMMMBBBBBEEERRRRSS');
              resolve(groupMembers);
            }
          })
        });
      });
    });
  }
  // I am not the only Admin in this group
  else if (me && me.groupRole === groupRole.ADMIN.value && adminNumber.length > 1) {
    console.log('We are ' + adminNumber.length + ' admins in this group');
    // first remove myself from the group 'users' and then from my groups
    Group.findOneAndUpdate({ _id: groupId }, {$pull: {users: {userId: myId}}}, { new: true }, function(err, group) {
      if (err) {
        reject(err);
      }
      console.log(group);
      User.findOneAndUpdate({ _id: myId }, {$pull: {myGroups: {groupId: groupId}}}, { new: true }, function(err, me) {
        if (err) {
          reject(err);
        }
        console.log(me);
        resolve('success');
      });
    });
  }
  // I am not admin and not alone on the group
  else if (me && me.groupRole === groupRole.PARTICIPANT.value) {
    console.log('I am not admin, we are ' + group.users.length + ' remaining on this group');
    // first remove myself from the group 'users' and then from my groups
    Group.findOneAndUpdate({ _id: groupId }, {$pull: {users: {userId: myId}}}, { new: true }, function(err, group) {
      if (err) {
        reject(err);
      }
      console.log(group);
      User.findOneAndUpdate({ _id: myId }, {$pull: {myGroups: {groupId: groupId}}}, { new: true }, function(err, me) {
        if (err) {
          reject(err);
        }
        console.log(me);
        resolve('success');
      });
    });
  }
}
}
})
});
};

*/
