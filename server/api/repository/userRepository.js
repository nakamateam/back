const mongoose = require('mongoose');
const User = mongoose.model('User');
const ChatMessage = mongoose.model('ChatMessage');
const uuidv1 = require('uuid/v1');


exports.findById = function(id) {
  return new Promise(function(resolve, reject) {
    User.findById(id, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.find = function() {
  return new Promise(function(resolve, reject) {
    User.find({}, {avatarUrl:1, username:1}, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.save = function(user) {
  return new Promise(function(resolve, reject) {
    user.save(function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};


exports.findOneByIdentifiant = function(user) {
  return new Promise(function(resolve, reject) {
    User.findOne({username: user.username}, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.findOneAndUpdate = function(id, nakamAvatar){
  return new Promise(function(resolve, reject) {
    User.findOneAndUpdate({ _id: id }, {$set:{hasAvatar:nakamAvatar}}, { new: true }, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.findOneAndBlockUser = function(id, pw, email){
  return new Promise(function(resolve, reject) {
    User.findOneAndUpdate({ _id: id }, {$set:{hashPassword: pw, email: email}}, { new: true }, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.findOneAndUpdateAvatarIsTrue = function(id, avatarUrl){
  return new Promise(function(resolve, reject) {
    User.findOneAndUpdate({ _id: id }, {$set:{hasAvatar:true, avatarUrl: avatarUrl}}, { new: true }, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.findOneAndUpdateAvatarIsFalse = function(id, avatarUrl){
  return new Promise(function(resolve, reject) {
    console.log('User.avatarUrl.default');
    console.log(User.avatarUrl.default);
    User.findOneAndUpdate({ _id: id }, {$set:{hasAvatar:false, avatarUrl: avatarUrl}}, { new: true }, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

function updateChatMessageUsername(userId, newNakaName) {
    ChatMessage.update({userId:userId}, {$set: {username:newNakaName }},{multi:true}, function (err, deletedChatConv) {
    if (err) {
      console.log(err)
    } else {
      console.log('___________________________deletedChatConv___________________________');
    }
  });
}

exports.findOneAndUpdateName = function(id, newNakaName){
  return new Promise(function(resolve, reject) {
    User.findOneAndUpdate({ _id: id }, {$set:{username:newNakaName}}, { new: true }, function(err, user) {
      if (err) {
        reject(err);
      }
      updateChatMessageUsername(id, newNakaName);
      resolve(user);
    });
  });
};

exports.findOneAndUpdateEmail = function(id, newNakaEmail){
  return new Promise(function(resolve, reject) {
    User.findOneAndUpdate({ _id: id }, {$set:{email:newNakaEmail}}, { new: true }, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.findOneAndUpdatePw = function(id, newNakaPw){
  return new Promise(function(resolve, reject) {
    User.findOneAndUpdate({ _id: id }, {$set:{hashPassword:newNakaPw}}, { new: true }, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};

exports.remove = function(id) {
  return new Promise(function(resolve, reject) {
    User.remove({ _id: id }, function(err, user) {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
};
