const mongoose = require('mongoose');
const User = mongoose.model('User');
const Group = mongoose.model('Group');
const File = mongoose.model('File');
const Event = mongoose.model('Event');
const Notification = mongoose.model('Notification');
const notifEnum = require('../models/notificationEnum');

exports.getMyNotifications = function(notifNumber, myId) {
  return new Promise(function(resolve, reject) {
    Notification.find({'receiver': myId}, function(err, myNotifications){
        if (err) {
          reject(err);
        } else {
          console.log('----------------------------GetMyNotifications--------------------------');
          console.log(myNotifications);
          if (myNotifications.length !== 0) {
            console.log('myNotifIsNotequalToZero');
            let notificationsProcess = myNotifications.map(function (notif, i) {
              return new Promise(function(resolve, reject) {
                if (notif.notificationType === notifEnum.NEWGROUPINVITATION.value ||
                  notif.notificationType === notifEnum.NEWFILEUPLOADED.value ||
                  notif.notificationType === notifEnum.NEWGROUPNAME.value ||
                  notif.notificationType === notifEnum.NEWGROUPAVATAR.value ||
                  notif.notificationType === notifEnum.NEWCALENDAREVENT.value ||
                  notif.notificationType === notifEnum.CALENDAREVENTEDITED.value) {
                  console.log('----------------------------'+notif.notificationType+'--------------------------');
                  /** First add sender avatarUrl and username to the notification */
                  User.findById(notif.sender, {_id:0, username: 1, avatarUrl: 1}, function(err, user) {
                    if (err) {
                      console.log(err);
                      reject(err);
                    }
                    if (user === null || user === undefined) {
                      console.log('---------'+notif.sender+' seems to not exists any longer----------------');
                      /** delete the notification with Id and add toRemove field to filter myNotifications */
                      Notification.deleteOne({_id:notif._id}, function (err, deletedNotif) {
                        if (err) {
                          reject(err);
                        } else {
                          notif.toRemove = true;
                          resolve(myNotifications);
                        }
                      });
                    } else {
                      notif.senderInfo = user;
                      console.log('----------------------------MYSENDERINFO IS :'+notif+'--------------------------');
                      /** Then add groupName and group avatarUrl */
                      Group.findById(notif.metaData.groupId, {groupName: 1, avatarUrl: 1}, function(err, group) {
                        if (err) {
                          reject(err);
                        }
                        if(group === null || group === undefined) {
                          /** delete the notification with Id and add toRemove field to filter myNotifications */
                          Notification.deleteOne({_id:notif._id}, function (err, deletedNotif) {
                            if (err) {
                              reject(err);
                            } else {
                              notif.toRemove = true;
                              resolve(myNotifications);
                            }
                          });
                        } else {
                          notif.groupInfo = group;
                          /** if newFileUploaded or newGroupAvatar notification check if the file still exists */
                          if ((notif.notificationType === notifEnum.NEWFILEUPLOADED.value || notif.notificationType === notifEnum.NEWGROUPAVATAR.value) && notif.metaData.fileId) {
                            File.findById(notif.metaData.fileId, function(err, file) {
                              if (err) {
                                reject(err);
                              }
                              if(file === null || file === undefined) {
                                /** delete the notification with Id and add toRemove field to filter myNotifications */
                                Notification.deleteOne({_id:notif._id}, function (err, deletedNotif) {
                                  if (err) {
                                    reject(err);
                                  } else {
                                    notif.toRemove = true;
                                    resolve(myNotifications);
                                  }
                                });
                              } else {
                                resolve(myNotifications);
                              }
                            });
                            /** if newCalendarEvent notification check if the event still exists */
                          } else if ((notif.notificationType === notifEnum.NEWCALENDAREVENT.value || notif.notificationType === notifEnum.CALENDAREVENTEDITED.value) && notif.metaData.eventId) {
                            Event.findById(notif.metaData.eventId, function(err, event) {
                              if (err) {
                                reject(err);
                              }
                              if(event === null || event === undefined) {
                                /** delete the notification with Id and add toRemove field to filter myNotifications */
                                Notification.deleteOne({_id:notif._id}, function (err, deletedNotif) {
                                  if (err) {
                                    reject(err);
                                  } else {
                                    notif.toRemove = true;
                                    resolve(myNotifications);
                                  }
                                });
                              } else {
                                notif.metaData.eventStartDay = event.start;
                                notif.metaData.eventTitle = event.title;
                                resolve(myNotifications);
                              }
                            });
                          } else {
                            resolve(myNotifications);
                          }
                        }
                      });
                    }
                  });
                }
                if (notif.notificationType === notifEnum.NAKAMAREQUEST.value || notif.notificationType === notifEnum.NEWNAKAMACONFIRMATION.value) {
                  /** First add sender avatarUrl and username the the notification */
                  User.findById(notif.sender, {_id:0, username: 1, avatarUrl: 1}, function(err, user) {
                    if (err) {
                      console.log(err);
                      reject(err);
                    }
                    if (user === null || user === undefined) {
                      /** delete the notification with Id and add toRemove field to filter myNotifications */
                      Notification.deleteOne({_id:notif._id}, function (err, deletedNotif) {
                        console.log(deletedNotif);
                        if (err) {
                          reject(err);
                        } else {
                          notif.toRemove = true;
                          resolve(myNotifications);
                        }
                      });
                    } else {
                      notif.senderInfo = user;
                        resolve(myNotifications);
                    }
                  });
                }
              });
            });
            /** Wait to receive all promise from .map iteration*/
            Promise.all(notificationsProcess)
              .then(function() {
                console.log('___________PROMISE ALL_______________');
                console.log(notificationsProcess);
                console.log(myNotifications);
                resolve(myNotifications.filter(notif => !notif.toRemove));
              })
              .catch(function (err) {
                reject(err);
              });
          } else {
            console.log('NoNotif');
            resolve(myNotifications);
          }
        }
      }).sort({_id:-1}).limit(Number(notifNumber)).lean()
  });
};

exports.updateMyNotificationsById = function(notifIds) {
  console.log(notifIds);
  return new Promise(function(resolve, reject) {
    Notification.update({ _id: { $in: notifIds }}, { $set: { unRead : false } },{ multi: true }
      , function(err, myNotifications){
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(myNotifications);
        }
      })
  });
};

