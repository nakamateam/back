const mongoose = require('mongoose');
const multer = require('multer');
const Avatar = mongoose.model('Avatar');
const ChatMessage = mongoose.model('ChatMessage');
const User = mongoose.model('User');
const fs = require('fs');
const path = require('path');
const filePath = path.dirname(require.main.filename);
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/avatar/')
  },
  filename: function (req, file, cb) {
    const ext = file.mimetype.split('/')[1];
    cb(null, file.fieldname + '_' + Date.now() + '.' + ext);
  }
});
const limits = { fileSize: 10 * 1024 * 1024 };
const upload = multer({
  storage: storage,
  limits: limits, //10Mb
}).single('avatar');
const imagemin = require('imagemin');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

exports.saveAvatar = function (req, res) {
  return new Promise(function (resolve, reject) {
    upload(req, res, function (err) {
        if (err) {
          reject(err);
          console.log(err);
        }
        User.findById(req.body.nakId, function (err, user) {
          if (err) {
            reject(err);
          }
          if (user.hasAvatar) {
            module.exports.removeById(req.body.nakId)
              .then(function (avatar) {
                module.exports.createAvatarAndUpdateUser(req)
                  .then(function (avatar) {
                    res.status(200).end('Avatar is uploaded');
                  }, function (error) {
                    res.status(400).send(error);
                  })
              }, function (error) {
                console.log(error);
                res.status(400).send(error);
              });
          } else {
            module.exports.createAvatarAndUpdateUser(req)
              .then(function (avatar) {
                res.status(200).end('Avatar is uploaded');
              }, function (error) {
                res.status(400).send(error);
              })
          }
        });
      }
    );
  });
};

function updateChatMessageAvatarUrl(userId, avatarUrl) {
  ChatMessage.update({userId:userId}, {$set: {userAvatar:avatarUrl }},{multi:true}, function (err, updateChatMessages) {
    if (err) {
      console.log(err)
    } else {
      console.log('___________________________updateChatMessages___________________________');
    }
  });
}

exports.createAvatarAndUpdateUser = function (req) {
  return new Promise(function (resolve, reject) {
    const newAvatar = new Avatar(req.file);
    newAvatar.save(function (err, avatar) {
      if (err) {
        console.log(err);
        reject(err);
      }
      console.log(avatar);
      (async () => {
        await imagemin([avatar.path], 'public/avatar', {
          use: [
            imageminPngquant(),
            imageminMozjpeg(imageminMozjpeg({
              quality: 10
            })),
          ]
        });
        console.log('avatar optimized');
      })();

      const avatarUrl = process.env.SERVER_URL + avatar.path;
      User.findOneAndUpdate({_id: req.body.nakId}, {
        $set: {
          hasAvatar: true,
          avatarUrl: avatarUrl
        }
      }, {new: true}, function (err, user) {
        if (err) {
          reject(err);
        }
        updateChatMessageAvatarUrl(req.body.nakId, avatarUrl);
        resolve('File has been successfully uploaded');
      });
    })
  });
};

exports.removeById = function (nakId) {
  return new Promise(function (resolve, reject) {
    let avatarFileName;
    Avatar.findOne({originalname: nakId}, function (err, avatar) {
      if (err) {
        reject(err);
      }
      avatarFileName = avatar.filename;
      Avatar.remove({originalname: nakId}, function (err, avatar) {
        if (err) {
          reject(err);
        }
        fs.unlink(filePath + '/public/avatar/' + avatarFileName, (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          } else {
            resolve(avatar);
          }
        });
      });
    });
  });
};

exports.resetAvatarById = function (req) {
  return new Promise(function (resolve, reject) {
    let avatarFileName;
    Avatar.findOne({originalname: req.user._id}, function (err, avatar) {
      if (err) {
        reject(err);
      }
      avatarFileName = avatar.filename;
      Avatar.remove({originalname: req.user._id}, function (err, avatar) {
        if (err) {
          reject(err);
        }
        fs.unlink(filePath + '/public/avatar/' + avatarFileName, (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          } else {
            User.findOneAndUpdate({_id: req.user._id}, {
              $set: {
                hasAvatar: false,
                avatarUrl: process.env.DEFAULT_AVATAR_URL
              }
            }, {new: true}, function (err, user) {
              if (err) {
                reject(err);
              }
              updateChatMessageAvatarUrl(req.user._id, process.env.DEFAULT_AVATAR_URL);
              resolve('Avatar has been successfully deleted');
            });
          }
        });
      });
    });
  });
};

exports.findAvatarByOriginalName = function (req, res) {
  return new Promise(function (resolve, reject) {
    //console.log(req.params.nakamid);
    if (req.user._id === 'undefined') {
      reject();
    } else {
      /** Check if file exists */
      Avatar.findOne({originalname: req.user._id}, function (err, avatar) {
        if (err) {
          reject(err);
        }
        if (!avatar) {
          reject();
        } else {
          res.set('Content-Type', avatar.mimetype);
          res.set('Content-Disposition', 'attachment');
          res.sendFile(filePath + '/public/avatar/' + avatar.filename, function (err) {
            if (err) {
              console.log(err);
              reject(err);
            }
          });
        }
      });
    }
  });
};

exports.streamMyFiles = function (filename, res) {
  return new Promise(function (resolve, reject) {
    //resolve(fs.createReadStream(filePath +'/public/files/' + filename).pipe(res));
    fs.readFile(filePath +'/public/avatar/' + filename, function (err, content) {
      if (err) {
        res.writeHead(400, {'Content-type':'text/html'});
        console.log(err);
        res.end("No such image");
      } else {
        //specify the content type in the response will be an image
        //res.set('Content-Type', avatar.mimeptype);
        res.set('Content-Disposition', 'attachment');
        res.end(content);
      }
    });
  });
};

/** send photo using fs.createRead Stream
 fs.createReadStream(filePath +'/public/avatar/' + avatar.filename).pipe(res);
 fs.readFile(filePath +'/public/avatar/' + avatar.filename, function (err, content) {
        if (err) {
          res.writeHead(400, {'Content-type':'text/html'});
          console.log(err);
          res.end("No such image");
        } else {
          //specify the content type in the response will be an image
          res.set('Content-Type', avatar.mimeptype);
          res.set('Content-Disposition', 'attachment');
          res.end(content);
        }
      });
 */
