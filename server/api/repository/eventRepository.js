const mongoose = require('mongoose');
const Group = mongoose.model('Group');
const User = mongoose.model('User');
const Event = mongoose.model('Event');
const Notification = mongoose.model('Notification');
const notifPrefEnum = require('../models/notifPreferenceEnum');
const groupRole = require('../models/groupRoleEnum');
const notifEnum = require('../models/notificationEnum');
const notifSocket = require('../../sockets/notifSocket');
const socketIO = require('../../helpers/socketio').get();
const NotificationSocket = new notifSocket(socketIO);
const alertEnum = require('../models/eventAlertEnum');
const schedule = require('node-schedule');

function handleNewCalendarEventNotification(event, group, myId) {
  console.log('inside handleNewCalendarEventNotification');
  User.findById(myId, {avatarUrl: 1, username: 1}, function (err, userData) {
    if (err) {
      reject(err);
      console.log(err);
    } else {
      console.log('_________userData____________');
      console.log(userData);
      group.users.forEach(function (user) {
        if (user.userId.toString() !== myId.toString() && user.notificationPreferences.calendar) {
          if (err) {
            reject(err);
            console.log(err);
          } else {
            let newCalendarEvent = new Notification({
              receiver: user.userId,
              sender: myId,
              created_at: new Date().toISOString(),
              notificationType: notifEnum.NEWCALENDAREVENT.value,
              metaData: {groupId: group._id, eventId: event._id}
            });
            newCalendarEvent.save(function (err, notif) {
              if (err) {
                reject(err);
              }
              notif.senderInfo = userData;
              notif.groupInfo = {groupName: group.groupName, avatarUrl: group.avatarUrl};
              notif.metaData.eventStartDay = event.start;
              NotificationSocket.sendLiveNotification(socketIO, user.userId.toString(), notif);
            });
          }
        }
      });
    }
  });
}

function startingIn(eventAlert) {
  switch (eventAlert) {
    case alertEnum.ONTIME.value:
      return 'now';
    case alertEnum.FIVEMIN.value:
      return 'in five minutes';
    case alertEnum.FIFTEENMIN.value:
      return 'in fifteen minutes';
    case alertEnum.THIRTYMIN.value:
      return 'in thirty minutes';
    case alertEnum.ONEHOUR.value:
      return 'in one hour';
    case alertEnum.ONEDAY.value:
      return 'in one day';
    case alertEnum.TWODAYS.value:
      return 'in two days';
  }
}

function handleCalendarEventNotification(event, group) {
  group.users.forEach(function (user) {
    if (user.notificationPreferences.calendar) {
      let newCalendarEventLiveNotif = {};
      newCalendarEventLiveNotif.notificationType = notifEnum.NEWCALENDAREVENT.value;
      newCalendarEventLiveNotif.groupInfo = {groupName: group.groupName, avatarUrl: group.avatarUrl};
      newCalendarEventLiveNotif.metaData = {alertType: startingIn(event.alert), groupId : group._id, eventTitle: event.title, eventId: event._id};
      console.log('_____________________newCalendarEventLiveNotif________________');
      console.log(newCalendarEventLiveNotif);
      NotificationSocket.sendLiveAlertNotification(socketIO, user.userId.toString(), newCalendarEventLiveNotif);
    }
  });
}

function handleAlertDate(event) {
  console.log(event.alert);
  const MS_PER_MINUTE = 60000;
  switch (event.alert) {
    case alertEnum.ONTIME.value:
      return new Date(event.start);
    case alertEnum.FIVEMIN.value:
      return new Date(event.start - 5 * MS_PER_MINUTE);
    case alertEnum.FIFTEENMIN.value:
      return new Date(event.start - 15 * MS_PER_MINUTE);
    case alertEnum.THIRTYMIN.value:
      return new Date(event.start - 30 * MS_PER_MINUTE);
    case alertEnum.ONEHOUR.value:
      return new Date(event.start - 60 * MS_PER_MINUTE);
    case alertEnum.ONEDAY.value:
      return new Date(event.start - 60 * 24 * MS_PER_MINUTE);
    case alertEnum.TWODAYS.value:
      return new Date(event.start - 60 * 24 * 2 * MS_PER_MINUTE);
  }
}

function scheduleCalendarEventAlertNotification(event, group) {
  console.log('inside handleNewCalendarEventAlertNotification');
  /** prevent to create a job with an alert Date which has already occured */
  if (handleAlertDate(event) > new Date()) {
    console.log('un nouveau job a été planifié');
    schedule.scheduleJob(event._id.toString(), handleAlertDate(event), function() {
      console.log(event.title);
      console.log(new Date(event.start));
      handleCalendarEventNotification(event, group)
    });
  }
}

function handleScheduledAlertNotification(event, group, myId) {
  const my_old_job = schedule.scheduledJobs[event._id.toString()];
  console.log('_________________scheduledJobs____________________');
  console.log(schedule.scheduledJobs);
  console.log('____________________my_old_job__________________');
  console.log(my_old_job);
  /** alert Date has been modified so first cancel the old job */
  if(my_old_job) {
    my_old_job.cancel();
  }
  if (event.alert !== alertEnum.NONE.value) {
    /** Then reschedule a new one */
    scheduleCalendarEventAlertNotification(event,group);
  }
}

function handleEventEditedNotification(event, group, myId) {
  /** delete edited event notification for a same event to avoid too many edited event notification */
  Notification.deleteMany({'metaData.eventId':event._id, notificationType: notifEnum.CALENDAREVENTEDITED.value}, function (err, deletedNotif) {
    if (err) {
      console.log(err)
    } else {
      /** then generate a new notification */
      group.users.forEach(function (user) {
        if (user.userId.toString() !== myId.toString() && user.notificationPreferences.calendar) {
          User.findById(user.userId, {avatarUrl:1, username:1}, function (err, user) {
            if (err) {
              reject(err);
              console.log(err);
            } else {
              let editedCalendarEvent = new Notification({
                receiver: user._id,
                sender: myId,
                created_at: new Date().toISOString(),
                notificationType: notifEnum.CALENDAREVENTEDITED.value,
                metaData: {groupId : group._id, eventId: event._id}
              });
              editedCalendarEvent.save(function (err, notif) {
                if (err) {
                  reject(err);
                }
                notif.senderInfo = user;
                notif.groupInfo = {groupName: group.groupName, avatarUrl: group.avatarUrl};
                notif.metaData.eventTitle = event.title;
                NotificationSocket.sendLiveNotification(socketIO, user._id.toString(), notif);
              });
            }
          });
        }
      });
    }
  });
}

exports.save = function (event, myId, res) {
  return new Promise(function (resolve, reject) {
    Group.findById(event.groupId, function (err, group) {
      if (err) {
        reject(err);
      }
      let myGroupRole = group.users.find(function (element) {
        return element.userId.toString() === myId.toString();
      });
      if (!myGroupRole) {
        res.status(405).send('It seems you do not belong to the group');
      } else  {
        const newEvent = new Event(event);
        newEvent.creationDate = new Date();
        newEvent.eventCreatorId = myId;
        newEvent.save(function (err, event) {
          if (err) {
            reject(err);
          }
          /** create Notification following user preferences if at least one user in the group has subscribed to calendar */
          if (group.users.filter(user => user.notificationPreferences.calendar).length > 0) {
            handleNewCalendarEventNotification(event,group, myId);
          }
          /** Handle job scheduler if ALERT has been fixed */
          if (event.alert !== alertEnum.NONE.value ) {
            scheduleCalendarEventAlertNotification(event,group);
          }
          console.log(event);
          resolve(event);
        });
      }
    });
  });
};

exports.resetEventAlert = function () {
  return new Promise(function (resolve, reject) {
    Event.find({start:{ $gte: new Date() }}, function (err, events) {
      if (err) {
        reject(err);
      }
      if (events && events.length > 0) {
        events.forEach(function (event) {
          if (event.alert !== alertEnum.NONE.value) {
            Group.findById(event.groupId, function (err, group) {
              if (err) {
                reject(err);
              } else {
                scheduleCalendarEventAlertNotification(event,group);
              }
            })
          }
        });
      }
    });
  });
};

exports.getEvents = function (myId, groupId) {
  return new Promise(function (resolve, reject) {
    Group.findOne({'_id': groupId, 'users.userId': myId}, {hasAvatar: 0, creatorId: 0}, function (err, group) {
      if (err) {
        console.log(err);
        reject(err);
      }
      if (group === undefined || group === null) {
        reject('Are you sure to be part of this group');
      } else {
        Event.find({'groupId': groupId}, function (err, events) {
          if (err) {
            console.log(err);
            reject(err);
          } else {
            console.log(events);
            resolve(events);
          }
        });
      }
    });
  });
};

exports.findOneAndUpdate = function(myId, event){
  return new Promise(function(resolve, reject) {
    Group.findOne({'_id': event.groupId, 'users.userId': myId}, {hasAvatar: 0, creatorId: 0}, function (err, group) {
      if (err) {
        console.log(err);
        reject(err);
      }
      if (group === undefined || group === null) {
        reject('Are you sure to be part of this group');
      } else {
        Event.findById(event._id, function(err, oldEvent) {
          if (err){
            reject(err);
          }
          Event.findOneAndUpdate({ _id: event._id }, event, { new: true }, function(err, event) {
            if (err){
              reject(err);
            }
            /** create Notification following user preferences if at least one user in the group has subscribed to calendar */
            if (group.users.filter(user => user.notificationPreferences.calendar).length > 0) {
              handleEventEditedNotification(event,group, myId);
            }
            /** Handle job scheduler if event.alert has been modified */
            if (oldEvent.alert !== event.alert || oldEvent.start !== event.start ) {
              handleScheduledAlertNotification(event,group, myId);
            }
            resolve(event);
          });
        });
      }
    });
  });
};

function deleteEventRelatedNotification(eventId) {
  Notification.deleteMany({'metaData.eventId':eventId}, function (err, deletedNotif) {
    if (err) {
      console.log(err)
    } else {
      console.log('___________________________deletion succeeded___________________________');
    }
  });
}

exports.deleteEvent = function(myId, eventId){
  return new Promise(function(resolve, reject) {
    Event.remove({ _id: eventId }, function(err, event) {
      if (err){
        reject(err);
      }
      deleteEventRelatedNotification(eventId);
      resolve(event);
    });
  });
};
