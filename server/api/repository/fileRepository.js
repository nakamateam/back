const mongoose = require('mongoose');
const multer = require('multer');
const File = mongoose.model('File');
const User = mongoose.model('User');
const Group = mongoose.model('Group');
const notifEnum = require('../models/notificationEnum');
const fs = require('fs');
const path = require('path');
const filePath = path.dirname(require.main.filename);
const notifSocket = require('../../sockets/notifSocket');
const socketIO = require('../../helpers/socketio').get();
const Notification = mongoose.model('Notification');
const NotificationSocket = new notifSocket(socketIO);
const ChatMessage = mongoose.model('ChatMessage');
const maxGroupSize = 1000 * 1024 * 1024; //1GB
const imagemin = require('imagemin');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/files/')
  },
  filename: function (req, file, cb) {
    const ext = file.mimetype.split('/')[1];
    cb(null, file.fieldname + '_' + Date.now() + '.' + getExtension(file.originalname));
  }
});
const limits = { fileSize: 20 * 1024 * 1024 };
function getExtension(filename) {
  let parts = filename.split('.');
  return parts[parts.length - 1];
}
const upload = multer({
  storage: storage,
  limits: limits, //20Mb
}).single('file');

function isImage(filename) {
  const ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
    case 'jpeg':
      return true;
  }
  return false;
}

function handleNewFileUploadNotification(group, req, wayId, file) {
  group.users.forEach(function (user) {
    if (user.userId.toString() !== req.user._id.toString() && user.notificationPreferences.sharedFiles) {
      User.findById(user.userId, {avatarUrl:1, username:1}, function (err, user) {
        if (err) {
          reject(err);
          console.log(err);
        } else {
          let newFileUploaded = new Notification({
            receiver: user._id,
            sender: req.user._id,
            created_at: new Date().toISOString(),
            notificationType: notifEnum.NEWFILEUPLOADED.value,
            metaData: {groupId : wayId, fileId:file._id, isImage: isImage(file.originalname), filename: file.filename}
          });
          newFileUploaded.save(function (err, notif) {
            if (err) {
              reject(err);
            }
            notif.senderInfo = user;
            notif.groupInfo = {groupName: group.groupName, avatarUrl: group.avatarUrl};
            NotificationSocket.sendLiveNotification(socketIO, user._id.toString(), notif);
          });
        }
      });
    }
  });
}

/** get groupSize */
function updateGroupSize(groupId) {
  File.aggregate([
    {$match: {groupId: mongoose.Types.ObjectId(groupId)}},
    {
      $group: {
        _id: null,
        totalAmount: {
          $sum: "$size"
        }
      }
    }
  ], function (err, totalGroupSize) {
    if (err) {
      console.log(err);
    } else {
      if (totalGroupSize && totalGroupSize[0].totalAmount) {
        Group.findOneAndUpdate({_id: groupId}, {$set: {groupSize: totalGroupSize[0].totalAmount}}, { new: true }, function(err, updatedGroup) {
          if (err) {
            console.log(err);
          }
        });
      }
    }
  });
}

exports.saveFile = function (req, res) {
  return new Promise(function (resolve, reject) {
    const wayId = req.headers['way-id'];
    if (wayId) {
      Group.findOne({'_id': wayId, 'users.userId': req.user._id}, {groupName: 1, users: 1, avatarUrl: 1, groupSize:1}, function (err, group) {
        if (err) {
          console.log(err);
          reject(err);
        }
        if (group === undefined || group === null) {
          reject('Are you sure to be part of this group');
        } else if (group.groupSize && group.groupSize >= maxGroupSize) {
          console.log('plus de place');
          reject('Sorry, NakamaWay is free and your way has reached its storage limit. Process to a small spring cleaning');
        } else {
          upload(req, res, function (err) {
              if (err) {
                console.log(err);
                reject(err);
              } else {
                let newFile = new File(req.file);
                newFile.postedBy = req.user._id;
                newFile.groupId = req.body.wayId;
                console.log(req.file);
                if (isImage(req.file.originalname)) {
                  (async () => {
                    await imagemin([req.file.path], 'public/files', {
                      use: [
                        imageminPngquant(),
                        imageminMozjpeg(imageminMozjpeg({
                              quality: 10
                            })),
                      ]
                    });

                    console.log('new image uploaded');
                  })();
                }
                newFile.save(function (err, file) {
                  if (err) {
                    console.log(err);
                    reject(err);
                  } else {
                    updateGroupSize(wayId);
                    /** create Notification following user preferences if at least one user in the group has subscribed */
                    if (group.users.filter(user => user.notificationPreferences.sharedFiles).length > 0) {
                      handleNewFileUploadNotification(group, req, wayId, file);
                    }
                    resolve(file);
                  }
                })
              }
            }
          );
        }
      });
    } else {
      reject('something went wrong with the group header');
    }
  });
};

exports.getMyGroupFiles = function (myId, groupId) {
  return new Promise(function (resolve, reject) {
        Group.findOne({'_id': groupId, 'users.userId': myId}, {hasAvatar: 0, creatorId: 0}, function (err, group) {
          if (err) {
            console.log(err);
            reject(err);
          }
          if (group === undefined || group === null) {
            reject('Are you sure to be part of this group');
          } else {
            File.find({'groupId': groupId, 'fieldname': 'file'},{fieldname:0, encoding: 0, destination: 0, groupId:0, path: 0}, function (err, myFiles) {
              if (err) {
                console.log(err);
                reject(err);
              } else {
                myFiles.forEach((file) => {
                  console.log('file');
                  console.log(typeof file.postedBy.toString());
                  console.log('myId');
                  console.log(typeof myId);
                  file.postedBy = file.postedBy.toString() === myId
                });
                console.log('Zeeeeeeeeeeeee Grouuuuuuuuuuuuuuuuup');
                console.log(myFiles);
                resolve(myFiles);
              }
            }).lean();
          }
        });
  });
};

exports.getFileInfo = function (myId, groupId, fileId) {
  return new Promise(function (resolve, reject) {
    Group.findOne({'_id': groupId, 'users.userId': myId}, {hasAvatar: 0, creatorId: 0}, function (err, group) {
      if (err) {
        console.log(err);
        reject(err);
      }
      if (group === undefined || group === null) {
        reject('Are you sure to be part of this group');
      } else {
        File.findById(fileId, {_id:0, fieldname: 0, encoding: 0, mimetype: 0, destination: 0, filename: 0, path: 0, groupId: 0}, function (err, fileInfo) {
          if (err) {
            reject(err);
          } else {
            User.findById(fileInfo.postedBy, {avatarUrl:1, username:1}, function(err, user) {
              if (err) {
                reject(err);
              } else {
                fileInfo.postedBy = user.username;
                fileInfo.postedByAvatarUrl = user.avatarUrl;
                resolve(fileInfo);
              }
            })
          }
        }).lean();
      }
    });
  });
};

exports.streamMyFiles = function (filename, res) {
  return new Promise(function (resolve, reject) {
    console.log(filename);
    //resolve(fs.createReadStream(filePath +'/public/files/' + filename).pipe(res));
    fs.readFile(filePath +'/public/files/' + filename, function (err, content) {
      if (err) {
        res.writeHead(400, {'Content-type':'text/html'});
        console.log(err);
        res.end("No such image");
      } else {
        //specify the content type in the response will be an image
        //res.set('Content-Type', 'image/gif');
        res.set('Content-Disposition', 'attachment');
        res.end(content);
      }
    });
  });
};

exports.streamMyMovie = function (req, res) {
  return new Promise(function (resolve, reject) {
    const path = filePath + '/public/files/' + req.params.filename;
    console.log('OUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUPPPPPPPPPPPPPPPPPSSSSSSS');
    console.log(path);
    const stat = fs.statSync(path);
    const fileSize = stat.size;
    const range = req.headers.range;
    if (range) {
      const parts = range.replace(/bytes=/, "").split("-");
      const start = parseInt(parts[0], 10);
      const end = parts[1]
        ? parseInt(parts[1], 10)
        : fileSize-1;
      const chunksize = (end-start)+1;
      const file = fs.createReadStream(path, {start, end})
      const head = {
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/mp4',
      };
      res.writeHead(206, head);
      file.pipe(res);
    } else {
      const head = {
        'Content-Length': fileSize,
        'Content-Type': 'video/mp4',
      };
      res.writeHead(200, head);
      fs.createReadStream(path).pipe(res);
    }
  });
};

function deleteFileRelatedNotification(fileId) {
  Notification.deleteMany({'metaData.fileId':fileId}, function (err, deletedNotif) {
    if (err) {
      console.log(err)
    } else {
      console.log('___________________________deletion succeeded___________________________');
    }
  });
}

function deleteChatRelatedMessage(fileId) {
  ChatMessage.findOne({'file.fileId':fileId}, function (err, chatMessage) {
    if (err) {
      console.log(err)
    } else {
      console.log('___________________________chatMessage___________________________');
      console.log(chatMessage);
      if (chatMessage) {
        if (chatMessage.file.length === 1) {
          ChatMessage.findOneAndDelete({'file.fileId':fileId}, function (err, file) {
            if (err) {
              console.log(err);
            }
          });
        }
        if (chatMessage.file.length > 1) {
          ChatMessage.findOneAndUpdate({'file.fileId':fileId}, {$pull: {file: {fileId: fileId}}}, { new: true }, function(err, updatedChatMessage) {
            if (err) {
              console.log(err);
            } else {
            }
          });
        }
      }

    }
  });
}

exports.removeFileAndUpdateById = function (fileId, wayId, myId, res) {
  return new Promise(function (resolve, reject) {
    Group.findOne({'_id': wayId, 'users.userId': myId}, function (err, group) {
      if (err) {
        console.log(err);
        reject(err);
      }
      if (group === undefined || group === null) {
        reject('Are you sure to be part of this group');
      } else {
        let myGroupRole = group.users.find(function (element) {
          return element.userId.toString() === myId.toString();
        });
        File.findOne({'_id': fileId}, function (err, file) {
          if (err) {
            reject(err);
          } else {
            if (myGroupRole.groupRole === 'ADMIN' || file.postedBy.toString() === myId) {
              let path;
              File.findOneAndDelete({'_id': fileId}, function (err, file) {
                if (err) {
                  reject(err);
                }
                updateGroupSize(file.groupId);
                deleteFileRelatedNotification(fileId);
                path = file.path;
                fs.unlink(filePath + '/' + path, (err) => {
                  if (err) {
                    console.log("failed to delete local file:" + err);
                  } else {
                    ChatMessage.findOne({'file.fileId':fileId}, function (err, chatMessage) {
                      if (err) {
                        console.log(err)
                      } else {
                        console.log('___________________________chatMessage___________________________');
                        console.log(chatMessage);
                        if (chatMessage) {
                          if (chatMessage.file.length === 1) {
                            ChatMessage.findOneAndDelete({_id: chatMessage._id}, function (err, chatMessageDeleted) {
                              if (err) {
                                console.log(err);
                              } else {
                                resolve({action: 'UPDATE', message: chatMessageDeleted});
                              }
                            });
                          }
                          if (chatMessage.file.length > 1) {
                            ChatMessage.findOneAndUpdate({_id: chatMessage._id}, {$pull: {file: {fileId: fileId}}}, { new: true }, function(err, updatedChatMessage) {
                              if (err) {
                                console.log(err);
                              } else {
                                resolve({action: 'UPDATE', message: updatedChatMessage});
                              }
                            });
                          }
                        }
                      }
                    });
                  }
                });
              });
            } else {
              res.status(405).send('It seems you do not have the right permission to perform this operation');
            }
          }
        });
      }
    });
  });
};

exports.removeById = function (fileId, wayId, myId, res) {
  return new Promise(function (resolve, reject) {
    Group.findOne({'_id': wayId, 'users.userId': myId}, function (err, group) {
      if (err) {
        console.log(err);
        reject(err);
      }
      if (group === undefined || group === null) {
        reject('Are you sure to be part of this group');
      } else {
        let myGroupRole = group.users.find(function (element) {
          return element.userId.toString() === myId.toString();
        });
        File.findOne({'_id': fileId}, function (err, file) {
          if (err) {
            reject(err);
          } else {
            if (myGroupRole.groupRole === 'ADMIN' || file.postedBy.toString() === myId) {
              let path;
              File.findOneAndDelete({'_id': fileId}, function (err, file) {
                if (err) {
                  reject(err);
                }
                deleteFileRelatedNotification(fileId);
                deleteChatRelatedMessage(fileId);
                updateGroupSize(file.groupId);
                path = file.path;
                fs.unlink(filePath + '/' + path, (err) => {
                  if (err) {
                    console.log("failed to delete local file:" + err);
                  } else {
                    resolve(file.originalname);
                  }
                });
              });
            } else {
              res.status(405).send('It seems you do not have the right permission to perform this operation');
            }
          }
        });
      }
    });
  });
};
