const notificationRepository = require('../repository/notificationRepository');

exports.getNotifications = function(req, res) {
  notificationRepository.getMyNotifications(req.params.notifNumber, req.user._id)
    .then(function(notifications) {
      res.json(notifications);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.updateNotifications = function(req, res) {
  notificationRepository.updateMyNotificationsById(req.body.nakamaIds)
    .then(function(notifications) {
      res.json(notifications);
    }, function(error) {
      res.status(400).send(error);
    });
};


