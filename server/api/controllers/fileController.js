let fileRepository = require("../repository/fileRepository");

exports.postDocument = function(req, res) {
  fileRepository.saveFile(req, res)
    .then(function(file){
      res.json(file);
    },function(error){
      res.status(400).send(error);
    })
};

exports.getMyGroupFiles = function(req, res) {
  fileRepository.getMyGroupFiles(req.user._id, req.params.groupId)
    .then(function(file) {
      res.json(file);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.getFileInfo = function(req, res) {
  fileRepository.getFileInfo(req.user._id, req.params.groupId, req.params.fileId)
    .then(function(file) {
      res.json(file);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.streamFileByName = function(req, res) {
  fileRepository.streamMyFiles(req.params.filename, res)
    .then(function(file) {
      res.json(file);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.streamMovieByName = function(req, res) {
  fileRepository.streamMyMovie(req, res)
    .then(function(file) {
      res.json(file);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.removeFileById = function(req, res) {
  fileRepository.removeById(req.body.fileId, req.body.wayId, req.user._id, res)
    .then(function(file) {
      res.json(file);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.removeFileAndUpdateById = function(req, res) {
  console.log(req.body.fileId);
  fileRepository.removeFileAndUpdateById(req.body.fileId, req.body.wayId, req.user._id, res)
    .then(function(file) {
      res.json(file);
    }, function(error) {
      res.status(400).send(error);
    })
};
/*

exports.resetAvatarById = function(req, res) {
  avatarRepository.resetAvatarById(req)
    .then(function(avatar){
      res.json({ message: 'Avatar successfully deleted' });
    },function(error){
      res.status(400).send(error);
    })
};

exports.getAvatarById = function(req, res) {
  avatarRepository.findAvatarByOriginalName(req, res)
    .then(function(avatar){
      res.json(avatar);
    },function(error){
      res.status(400).send({ message: 'bad nakama Id' });
    })
};
*/
