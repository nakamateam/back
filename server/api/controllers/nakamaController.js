const nakamaRepository = require('../repository/nakamaRepository');

exports.findNewNakamasAutocomplete = function(req, res) {
  nakamaRepository.AutocompletefindNewNakamas(req.user._id, req.body.query)
    .then(function(users) {
      res.json(users);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.getMyAcceptedNakamas = function(req, res) {
  nakamaRepository.getMyNakamas(req.user._id)
    .then(function(users) {
      res.json(users);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.getMyNakamas = function(req, res) {
  nakamaRepository.findMyNakamas(req.user._id)
    .then(function(nakamas) {
      res.json(nakamas);
    }, function(error) {
      res.status(400).send(error);
    })
};



exports.sendNakamaRequest = function(req, res) {
  nakamaRepository.sendNakamaRequest(req.user._id, req.body.nakamaId)
    .then(function(friend) {
      res.json(friend);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.acceptNakamaRequest = function(req, res) {
  nakamaRepository.acceptNakamaRequest(req.user._id, req.body.nakamaId)
    .then(function(friend) {
      res.json(friend);
    }, function(error) {
      if (error.noLongerValid) {
        res.status(403).send(error);
      } else {
        res.status(400).send(error);
      }
    });
};

exports.removeNakama = function(req, res) {
  nakamaRepository.removeNakamaFromNakamas(req.user._id, req.body.nakamaId)
    .then(function(avatar){
      res.json('Nakama successfully deleted');
    },function(error){
      res.status(400).send(error);
    })
};

exports.cancelRequest = function(req, res) {
  console.log(req.user._id, req.body.nakamaId);
  nakamaRepository.cancelRequestAndUpdateNotification(req.user._id, req.body.nakamaId)
    .then(function(nakama){
      res.json('Request successfully canceled');
    },function(error){
      res.status(400).send(error);
    })
};

/** THOSE ARE THE METHOD FOR CUSTOM NAKAMA RELATIONSHIP*/

/** Get users to add new nakamas : /newnakama  (get) */
/*exports.getNakamas = function(req, res) {
  nakamaRepository.findNewNakamas(req.user._id)
    .then(function(nakamas) {
      res.json(nakamas);
    }, function(error) {
      res.status(400).send(error);
    });
};*/

/** Get my nakamas and my pending Request : /nakamas */
/*
exports.getMyNakamas = function(req, res) {
  nakamaRepository.findMyNakamas(req.user._id)
    .then(function(nakamas) {
      return getNakamasData(nakamas);
    }, function(error) {
      res.status(400).send(error);
    })
    .then(function(nakamaDatas) {
    res.json(nakamaDatas);
    },function(error){
    res.status(400).send(error);
  });
};
};

let getNakamasData = function(nakamas) {
  return new Promise(function(resolve, reject) {
    nakamaRepository.getMyNakamasData(nakamas)
      .then(function(nakamas) {
        resolve(nakamas);
      }, function(error) {
        reject(error);
      });
  });
};

exports.addNewNakama = function(req, res) {
  console.log('loooooooool');
  nakamaRepository.findOneAndUpdate(req.body.nakamaId, req.user._id)
    .then(function(success) {
      res.json('Request sent successfully');
    }, function(error) {
      res.status(400).send(error);
    });
};
*/
