let avatarRepository = require("../repository/avatarRepository");

exports.postDocument = function(req, res) {
  avatarRepository.saveAvatar(req, res)
    .then(function(avatar){
      res.status(200).end('Avatar is uploaded');
    },function(error){
      res.status(400).send(error);
    })
};

exports.resetAvatarById = function(req, res) {
  avatarRepository.resetAvatarById(req)
    .then(function(avatar){
      res.json({ message: 'Avatar successfully deleted' });
    },function(error){
      res.status(400).send(error);
    })
};

exports.streamAvatarByName = function(req, res) {
  avatarRepository.streamMyFiles(req.params.filename, res)
    .then(function(avatar) {
      res.json(avatar);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.getAvatarById = function(req, res) {
  avatarRepository.findAvatarByOriginalName(req, res)
    .then(function(avatar){
      res.json(avatar);
    },function(error){
      res.status(400).send({ message: 'bad nakama Id' });
    })
};
