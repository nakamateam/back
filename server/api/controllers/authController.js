const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Cookies = require('cookies');
const uid = require('uid-safe');

const roles = require('../models/roleEnum');
const User = mongoose.model('User');
const userRepository = require('../repository/userRepository');

/** For password reset purpose*/
const async = require('async');
const crypto = require('crypto');
const path = require('path');
const  hbs = require('nodemailer-express-handlebars');
const email = process.env.MAILER_EMAIL;
const pass = process.env.MAILER_PASSWORD;
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');


const smtpTransport = nodemailer.createTransport({
  service: process.env.MAILER_SERVICE_PROVIDER,
  auth: {
    user: email,
    pass: pass
  }
});

const handlebarsOptions = {
  viewEngine: {
    extName: '.html',
    partialsDir: path.resolve('./server/api/templates/')
  },
  viewPath: path.resolve('./server/api/templates/'),
  extName: '.html'
};

smtpTransport.use('compile', hbs(handlebarsOptions));

/**
 * Secure the request against CSRF and XSS Attacks
 * return the xsrfToken
 */
const secureHttpXsrfToken = function(req, res, user) {
  // generate random token to protect CSRF
  const xsrfToken = uid.sync(18);

  // 12 hours
  const days = 1;
  const expires = days * 12 * 60 * 60 * 1000;
  const expirationDate = new Date();
  expirationDate.setTime(expirationDate.getTime() + expires);

  // create the JWT Token
  const jwtToken = initJwtToken(expires, user, xsrfToken);

  // create a cookie to protect XSS
  initCookie(req, res, jwtToken, expirationDate);

  return xsrfToken;
};

const secureIoToken = function(req, res, user) {
  // 12 hours
  const days = 1;
  const expires = days * 12 * 60 * 60 * 1000;
  const expirationDate = new Date();
  expirationDate.setTime(expirationDate.getTime() + expires);

  // create the IO Token
  return initIoToken(expires, user);
};

/**
 * Init the jwt Token
 * @param {Date} expires Date expiration of the jwt Token
 * @param {User} user  User data
 * @param {string} xsrfToken token to protect csrf
 */
const initJwtToken = function(expires, user, xsrfToken) {
  const options = {
    expiresIn: expires // 12 heures
  };

  return jwt.sign({
      _id: user._id,
      username: user.username,
      role: user.role,
      xsrfToken: xsrfToken },
    process.env.SECRET_KEY, options);
};

const initIoToken = function(expires, user) {
  const options = {
    expiresIn: expires // 12 heures
  };

  return jwt.sign({
      _id: user._id,
      username: user.username
    },
    process.env.SECRET_KEY, options);
};

/**
 * Init a cookie to send the JWT token to the client Side
 * @param {*} req
 * @param {*} res
 * @param {*} jwtToken
 * @param {*} expirationDate
 */
const initCookie = function(req, res, jwtToken, expirationDate) {
  // create a cookie to protect XSS
  const booleanHttps = process.env.HTTPS_ENABLED === 'true';

  new Cookies(req, res).set('nakamaToken', jwtToken , {
    httpOnly: true,
    secure: booleanHttps,
    expires: expirationDate
  });
};

exports.login = function(req, res) {
  if (req.body.username === 'undefined' || req.body.password === 'undefined') {
    return res.status(412).json({ message: 'Authentication failed. username and/or password is empty' });
  }
  userRepository.findOneByIdentifiant(req.body)
    .then(function(user) {
      if (!user) {
        return res.status(412).json({ message: 'Authentication failed. Wrong username or password' });
      }
      if (!user.comparePassword(req.body.password)) {
        return res.status(412).json({ message: 'Authentication failed. Wrong username or password' });
      } else {
        const xsrfToken =  secureHttpXsrfToken(req, res, user);
        const ioToken = secureIoToken(req, res, user);
        const userView = {
                          role: user.role,
                          avatarUrl: user.avatarUrl,
                          token: xsrfToken,
                          ioToken: ioToken
                          };
        return res.json(userView);
      }
    }, function(error) {
      res.status(401).json({ message: 'Authentication failed.' });
    });
};

exports.loginRequired = function(req, res, next) {
  if (req.user) {
    next();
  } else {
    return res.status(401).json({ message: 'Unauthorized user!' });
  }
};

exports.loginRequiredForStream = function(req, res, next) {
  const token = new Cookies(req, res).get('nakamaToken');
  if ( token) {
    jwt.verify(token, process.env.SECRET_KEY.trim(), function(err, decoded) {
      if (err) {
        return res.status(401).json({ message: 'Unauthorized user!' });
      } else {
        req.user = decoded;
        next();
      }
    });
  } else {
    return res.status(401).json({ message: 'Unauthorized user!' });
  }
};

/**
 * check the role of the user and verify is the user is connected
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.loginRequiredAndisAdmin = function(req, res, next) {
  if (req.user) {
    if (req.user.role !== roles.NAKAMADMIN.value) {
      return res.status(401).json({ message: 'Unauthorized user!' });
    }
    next();
  } else {
    return res.status(401).json({ message: 'Unauthorized user!' });
  }
};


exports.forgotPassword = function(req, res) {
  async.waterfall([
    function(done) {
      User.findOne({
        email: req.body.email
      }).exec(function(err, user) {
        if (user) {
          done(err, user);
        } else {
          return res.status(412).json({ message: 'Email not found. Are you sure this is the email with which you registered?' });
        }
      });
    },
    function(user, done) {
      // create the random token
      crypto.randomBytes(20, function(err, buffer) {
        const token = buffer.toString('hex');
        done(err, user, token);
      });
    },
    function(user, token, done) {
      User.findOneAndUpdate({ _id: user._id }, { resetPasswordToken: token, resetPasswordExpires: Date.now() + 86400000 }, { upsert: true, new: true }).exec(function(err, new_user) {
        done(err, token, new_user);
      });
    },
    function(token, user, done) {
      const data = {
        to: user.email,
        from: email,
        template: 'forgot-password-email',
        subject: 'Need a new Nakama password?',
        context: {
          url: `${process.env.CORS_ORIGIN}/resetpw?token=${token}`,
          name: user.username
        }
      };

      smtpTransport.sendMail(data, function(err) {
        if (!err) {
          return res.json({ message: 'Kindly check your email for further instructions' });
        } else {
          return done(err);
        }
      });
    }
  ], function(err) {
    return res.status(422).json({ message: err });
  });
};

exports.resetPassword = function(req, res) {
  User.findOne({
    resetPasswordToken: req.body.token,
    resetPasswordExpires: {
      $gt: Date.now()
    }
  }).exec(function(err, user) {
    if (!err && user) {
      if (req.body.newPassword === req.body.confirmPassword) {
        user.hashPassword = bcrypt.hashSync(req.body.newPassword, 10);
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;
        user.save(function(err) {
          if (err) {
            return res.status(422).send({
              message: err
            });
          } else {
            const data = {
              to: user.email,
              from: email,
              template: 'reset-password-email',
              subject: 'New Nakama Password Confirmation',
              context: {
                name: user.username
              }
            };

            smtpTransport.sendMail(data, function(err) {
              if (!err) {
                return res.json({ message: 'Password reset' });
              } else {
                return done(err);
              }
            });
          }
        });
      } else {
        return res.status(412).json({ message: 'Passwords do not match' });
      }
    } else {
      return res.status(412).json({ message: 'Password reset token is invalid or has expired' });
    }
  });
};

