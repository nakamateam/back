const eventRepository = require('../repository/eventRepository');

exports.postCalendarEvent = function(req, res) {
  eventRepository.save(req.body, req.user._id, res)
    .then(function(newEvent) {
      res.json(newEvent);
    }, function(error) {
      console.log(error);
      res.status(400).send(error);
    });
};

exports.getEvents = function(req, res) {
  eventRepository.getEvents(req.user._id, req.params.groupId)
    .then(function(file) {
      res.json(file);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.deleteEvent = function(req, res) {
  eventRepository.deleteEvent(req.user._id, req.params.eventId)
    .then(function(event){
      res.json({ message: 'Event successfully deleted' });
    },function(error){
      res.status(400).send(error);
    })
};

exports.updateEvent = function(req, res) {
  eventRepository.findOneAndUpdate(req.user._id, req.body)
    .then(function(event){
      res.json(event);
    },function(error){
      res.status(400).send(error);
    })
};
