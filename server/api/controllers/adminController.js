const mongoose = require('mongoose');
const adminRepository = require('../repository/adminRepository');
const Group = mongoose.model('Group');


exports.getnakamaWayGroups = function(req, res) {
  adminRepository.getnakamaWayGroups()
    .then(function(groups) {
      res.json(groups);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.getNakamaWayTotalUsers = function(req, res) {
  adminRepository.getNakamaWayTotalUsers()
    .then(function(totolUsers) {
      res.json(totolUsers);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.getMongoDbStats = function(req, res) {
  adminRepository.getMongoDbStats()
    .then(function(dbStats) {
      res.json(dbStats);
    }, function(error) {
      res.status(400).send(error);
    })
};
