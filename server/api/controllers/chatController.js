const chatRepository = require('../repository/chatRepository');

exports.getChatRoomMessages = function(req, res) {
  chatRepository.getChatRoomMessages(req.params.room, req.params.from, req.user._id)
    .then(function(room) {
      res.json(room);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.getChatLinks = function(req, res) {
  chatRepository.getChatLinks(req.params.room)
    .then(function(chatMessages) {
      res.json(chatMessages);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.getUnreadMesage = function(req, res) {
  chatRepository.getUnreadMesage(req.params.room, req.user._id)
    .then(function(unreadMessage) {
      res.json(unreadMessage);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.getUser = function(req, res) {
  chatRepository.findById(req.user._id)
    .then(function(user) {
      res.json(user);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.updateUnreadMessagesInRoom = function(req, res) {
  chatRepository.updateUnreadMessagesInRoom(req.body.roomId, req.user._id)
    .then(function(cleanUnreadStatus) {
      res.json(cleanUnreadStatus);
    }, function(error) {
      res.status(400).send(error);
    });
};
