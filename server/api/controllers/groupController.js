const mongoose = require('mongoose');
const groupRepository = require('../repository/groupRepository');
const Group = mongoose.model('Group');

exports.postGroup = function(req, res) {
  const newGroup = new Group(req.body);
  newGroup.creationDate = new Date();
  newGroup.creatorId = req.user._id;
  groupRepository.save(newGroup, req.user._id)
    .then(function(newGroup) {
      res.json(newGroup);
    }, function(error) {
      console.log(error);
      res.status(400).send(error);
    });
};

exports.findAGroupAutoComplete = function(req, res) {
  groupRepository.AutocompletefindAGroup(req.user._id, req.body.query)
    .then(function(groups) {
      res.json(groups);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.getMyGroups = function(req, res) {
  groupRepository.getMyGroups(req.user._id)
    .then(function(groups) {
      res.json(groups);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.postGroupAvatar = function(req, res) {
  groupRepository.saveAvatar(req, res)
    .then(function(avatarUrl){
      res.json(avatarUrl);
    },function(error){
      res.send(error);
    })
};

exports.putGroupName = function(req, res) {
  if (!req.body.newWayName || req.body.newWayName.length < 1 || req.body.newWayName.length > 50) {
    return res.status(412).json({ message: 'Your Way name should be between 1 and 50 characters max'});
  } else {
    groupRepository.saveGroupName(req.user._id, req.body.newWayName, req.body.wayId, res)
    .then(function(groupName){
      res.json(groupName);
    },function(error){
      res.send(error);
    })
  }
};

exports.putGroupNotifPreferences = function(req, res) {
  if (!req.body.wayId || !req.body.notifPrefType || req.body.newPrefValue === undefined) {
    return res.status(412).json({ message: 'It seems the request is not valid, if the problem persists contact your administrator'});
  } else {
    groupRepository.updateGroupNotifPreferences(req.user._id, req.body.wayId, req.body.notifPrefType, req.body.newPrefValue, res)
      .then(function(grouPref){
        res.json(grouPref);
      },function(error){
        res.send(error);
      })
  }
};

exports.makeParticipant = function(req, res) {
  if (!req.body.userId || !req.body.wayId) {
    return res.status(412).json({ message: 'It seems the request is not valid, if the problem persists contact your administrator'});
  } else {
    groupRepository.makeParticipant(req.user._id, req.body.userId, req.body.wayId, res)
      .then(function(groupName){
        res.json(groupName);
      },function(error){
        res.send(error);
      })
  }
};

exports.addNewMember = function(req, res) {
  if (!req.body.userId || !req.body.wayId) {
    return res.status(412).json({ message: 'It seems the request is not valid, if the problem persists contact your administrator'});
  } else {
    groupRepository.addNewMember(req.user._id, req.body.userId, req.body.wayId, res)
      .then(function(newMember){
        res.json(newMember);
      },function(error){
        res.send(error);
      })
  }
};

exports.makeAdmin = function(req, res) {
  if (!req.body.userId || !req.body.wayId) {
    return res.status(412).json({ message: 'It seems the request is not valid, if the problem persists contact your administrator'});
  } else {
    groupRepository.makeAdmin(req.user._id, req.body.userId, req.body.wayId, res)
      .then(function(groupName){
        res.json(groupName);
      },function(error){
        res.send(error);
      })
  }
};

exports.removeGroupUser = function(req, res) {
  if (!req.body.userId || !req.body.wayId) {
    return res.status(412).json({ message: 'It seems the request is not valid, if the problem persists contact your administrator'});
  } else {
    groupRepository.removeGroupUser(req.user._id, req.body.userId, req.body.wayId, res)
      .then(function(groupName){
        res.json(groupName);
      },function(error){
        res.send(error);
      })
  }
};

exports.lastGroupConnection = function(req, res) {
  if (!req.body.wayId) {
    return res.status(412).json({ message: 'It seems the request is not valid, if the problem persists contact your administrator'});
  } else {
    groupRepository.updateLastGroupConnection(req.user._id, req.body.wayId, new Date())
      .then(function(group){
        res.json(group);
      },function(error){
        res.send(error);
      })
  }
};

exports.removeAvatarByGroupId = function(req, res) {
  groupRepository.resetAvatarByGroupId(req.user._id, req.body.wayId, res)
    .then(function(avatarUrl) {
      res.json(avatarUrl);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.getMyGroupRole = function(req, res) {
  groupRepository.getMyGroupRole(req.user._id, req.params.id)
    .then(function(group) {
      res.json(group);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.getMyGroupInfo = function(req, res) {
  groupRepository.getMyGroupInfo(req.user._id, req.params.id)
    .then(function(group) {
      res.json(group);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.getMyGroupPreferences = function(req, res) {
  groupRepository.getMyGroupPreferences(req.user._id, req.params.id)
    .then(function(groupPref) {
      res.json(groupPref);
    }, function(error) {
      res.status(400).send(error);
    })
};

exports.searchNewMembersAutocomplete = function(req, res) {
  groupRepository.searchNewMembersAutocomplete(req.user._id, req.body.query, req.body.wayId)
    .then(function(users) {
      res.json(users);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.deleteGroup = function(req, res) {
  groupRepository.removeGroup(req.user._id, req.body.groupId)
    .then(function(group){
      res.json('group successfully deleted');
    },function(error){
      res.status(400).send(error);
    })
};
