const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const User = mongoose.model('User');
const userRepository = require('../repository/userRepository');
const kickbox = require('kickbox').client('live_adb770709848420d3cb2ba39b4a96faa3f9a461e2d0368970f418e6a34898981').kickbox();


exports.getUsers = function(req, res) {
  userRepository.find()
    .then(function(users) {
      res.json(users);
    }, function(error) {
      res.status(400).send(error);
    });
};


exports.postUser = function(req, res) {
  console.log(req);
  const newUser = new User(req.body);
  newUser.hashPassword = bcrypt.hashSync(req.body.password, 10);
  userRepository.save(newUser)
    .then(function(user) {
      res.json(user);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.getUser = function(req, res) {
  userRepository.findById(req.user._id)
    .then(function(user) {
      const userView = {
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        _id: user._id,
        role: user.role,
        hasAvatar: user.hasAvatar,
        email: user.email,
        avatarUrl: user.avatarUrl
      };
      res.status(200).json(userView);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.putUser = function(req, res) {
  console.log(req.body);
  userRepository.findOneAndBlockUser(req.body._id, req.body.password, req.body.email)
    .then(function(user) {
      res.json(user);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.putUserName = function(req, res) {
  if (req.body.newNakamaUsername === 'undefined' || req.body.currentPwForUsername === 'undefined') {
    return res.status(412).json({ message: 'Authentication failed. New username and/or password is empty' });
  }
  userRepository.findById(req.user._id)
    .then(function(user) {
      if (!user || user === null) {
        return res.status(412).json({ message: 'Authentication failed. Nakama not found' });
      }
      if (!user.comparePassword(req.body.currentPwForUsername)) {
        return res.status(412).json({ message: 'Authentication failed. Wrong password' });
      } else {
        userRepository.findOneAndUpdateName(req.user._id, req.body.newNakamaUsername)
          .then(function(user) {
            const userView = {
              username: user.username,
            };
            res.json(userView);
          }, function(error) {
            res.status(412).json({ message: 'This username is already taken' });
          });
      }
    }, function(error) {
      res.status(412).json({ message: 'Authentication failed.' });
    });
};

exports.putNakamAvatar = function(req, res) {
  userRepository.findOneAndUpdate(req.params.id, req.body.hasAvatar)
    .then(function(user) {
      res.json(user);
    }, function(error) {
      res.status(400).send(error);
    });
};

exports.putUserEmail = function(req, res) {
  if (req.body.newNakamaEmail === 'undefined' || req.body.currentPwForEmail === 'undefined') {
    return res.status(412).json({ message: 'Authentication failed. New email and/or password is empty' });
  }
  userRepository.findById(req.user._id)
    .then(function(user) {
      if (!user || user === null) {
        return res.status(412).json({ message: 'Authentication failed. Nakama not found' });
      }
      if (!user.comparePassword(req.body.currentPwForEmail)) {
        return res.status(412).json({ message: 'Authentication failed. Wrong password' });
      } else {
        kickbox.verify(req.body.newNakamaEmail, function (err, response) {
          if(err){
            res.status(400).send(err);
          }
          if (response.body.result === 'deliverable') {
            userRepository.findOneAndUpdateEmail(req.user._id, req.body.newNakamaEmail)
              .then(function(user) {
                const userView = {
                  email: user.email,
                };
                res.json(userView);
              }, function(error) {
                res.status(412).json({ message: 'This email is already taken' });
              });
          }
          if(response.body.result !== 'deliverable') {
            res.status(412).json({ message: 'Your email address is not valid' });
          }
          if (response.body.success === false || response.body.result === 'unknown') {
            res.status(412).json({ message: 'An error occured when checking the validity of your email, Please contact your admin' });
          }
        });
      }
    }, function(error) {
      res.status(412).json({ message: 'Authentication failed.' });
    });
};

exports.putUserPw = function(req, res) {
  if (req.body.currentNakamaPw === 'undefined' || req.body.newPw === 'undefined' || req.body.confirmNewPw === 'undefined') {
    return res.status(412).json({ message: 'Authentication failed. Current pw and/or new password is empty' });
  }
  if (req.body.newPw !== req.body.confirmNewPw) {
    return res.status(412).json({ message: 'Passwords do not match, Please try again' });
  }
  userRepository.findById(req.user._id)
    .then(function(user) {
      if (!user || user === null) {
        return res.status(412).json({ message: 'Authentication failed. Nakama not found' });
      }
      if (!user.comparePassword(req.body.currentNakamaPw)) {
        return res.status(412).json({ message: 'Authentication failed. Wrong password' });
      } else {
        const hashPassword = bcrypt.hashSync(req.body.newPw, 10);
        userRepository.findOneAndUpdatePw(req.user._id, hashPassword)
          .then(function(user) {
            const userView = {
              username: user.username,
            };
            res.json(userView);
          }, function(error) {
            res.status(400).json({ message: 'Oops, something went wrong' });
          });
      }
    }, function(error) {
      res.status(412).json({ message: 'Authentication failed.' });
    });
};


exports.deleteUser = function(req, res) {
  userRepository.remove(req.params.id)
    .then(function(user) {
      res.json({ message: 'User successfully deleted' });
    }, function(error) {
      res.status(400).send(error);
    });
};
