module.exports = function(app) {
  let fileApi = require('../controllers/fileController');
  const authApi = require('../controllers/authController');

  app.route('/file')
    .post(authApi.loginRequired, fileApi.postDocument)
    .put(authApi.loginRequired, fileApi.removeFileById);

  app.route('/chat-file')
    .put(authApi.loginRequired, fileApi.removeFileAndUpdateById);

  app.route('/fileinfo/:fileId/:groupId')
    .get(authApi.loginRequired, fileApi.getFileInfo);

  app.route('/file/:filename')
    .get(authApi.loginRequired, fileApi.streamFileByName);

  app.route('/filemovie/:filename')
    .get(authApi.loginRequiredForStream, fileApi.streamMovieByName);

  app.route('/files/:groupId')
    .get(authApi.loginRequired, fileApi.getMyGroupFiles);

};
