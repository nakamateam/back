module.exports = function(app) {
  const authApi = require('../controllers/authController');

  app.route('/login')
    .post(authApi.login);

  app.route('/forgotpw')
    .post(authApi.forgotPassword);

  app.route('/resetpw')
    .post(authApi.resetPassword);

};
