module.exports = function (app) {
  const groupApi = require('../controllers/groupController');
  const authApi = require('../controllers/authController');

  app.route('/groups')
    .get(authApi.loginRequired, groupApi.getMyGroups)
    .post(authApi.loginRequired, groupApi.postGroup);

  app.route('/group')
    .put(authApi.loginRequired, groupApi.deleteGroup)
    .post(authApi.loginRequired, groupApi.findAGroupAutoComplete);

  app.route('/new-connection')
    .put(authApi.loginRequired, groupApi.lastGroupConnection);

  app.route('/group-role/:id')
    .get(authApi.loginRequired, groupApi.getMyGroupRole);

  app.route('/group-avatar')
    .post(authApi.loginRequired, groupApi.postGroupAvatar)
    .put(authApi.loginRequired, groupApi.removeAvatarByGroupId);

  app.route('/group-name')
    .put(authApi.loginRequired, groupApi.putGroupName);

  app.route('/make-group-participant')
    .put(authApi.loginRequired, groupApi.makeParticipant);

  app.route('/make-group-admin')
    .put(authApi.loginRequired, groupApi.makeAdmin);

  app.route('/new-member')
    .put(authApi.loginRequired, groupApi.addNewMember);

  app.route('/remove-way-user')
    .put(authApi.loginRequired, groupApi.removeGroupUser);

  app.route('/group/:id')
    .get(authApi.loginRequired, groupApi.getMyGroupInfo);

  app.route('/group-preference/:id')
    .get(authApi.loginRequired, groupApi.getMyGroupPreferences);

  app.route('/group-preference')
    .put(authApi.loginRequired, groupApi.putGroupNotifPreferences);

  app.route('/search-new-member')
    .post(authApi.loginRequired, groupApi.searchNewMembersAutocomplete);
};
