module.exports = function (app) {
  const eventApi = require('../controllers/eventController');
  const authApi = require('../controllers/authController');

  app.route('/events')
    .post(authApi.loginRequired, eventApi.postCalendarEvent)
    .put(authApi.loginRequired, eventApi.updateEvent);

  app.route('/events/:groupId')
    .get(authApi.loginRequired, eventApi.getEvents);

  app.route('/event/:eventId')
    .delete(authApi.loginRequired, eventApi.deleteEvent);

};
