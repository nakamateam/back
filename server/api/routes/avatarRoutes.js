module.exports = function(app) {
  let avatarApi = require('../controllers/avatarController');
  const authApi = require('../controllers/authController');

  app.route('/upload')
    .post(authApi.loginRequired, avatarApi.postDocument);

  app.route('/avatar')
    .get(authApi.loginRequired, avatarApi.getAvatarById)
    .delete(authApi.loginRequired, avatarApi.resetAvatarById);

  app.route('/avatar/:filename')
    .get(authApi.loginRequired, avatarApi.streamAvatarByName);

};
