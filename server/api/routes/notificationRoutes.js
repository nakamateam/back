module.exports = function (app) {
  const notificationApi = require('../controllers/notificationController');
  const authApi = require('../controllers/authController');

  app.route('/notification/:notifNumber')
    .get(authApi.loginRequired, notificationApi.getNotifications);

  app.route('/notification')
    .put(authApi.loginRequired, notificationApi.updateNotifications);


};
