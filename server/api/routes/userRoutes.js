module.exports = function (app) {
  const userApi = require('../controllers/userController');
  const authApi = require('../controllers/authController');

  app.route('/users')
    .get(authApi.loginRequiredAndisAdmin, userApi.getUsers)
    .post(authApi.loginRequiredAndisAdmin, userApi.postUser);

  app.route('/user')
    .get(authApi.loginRequired, userApi.getUser)
    .delete(authApi.loginRequiredAndisAdmin, userApi.deleteUser)
    .put(authApi.loginRequiredAndisAdmin, userApi.putUser);

  app.route('/nakaname')
    .put(authApi.loginRequired, userApi.putUserName);

  app.route('/nakavatar')
    .put(authApi.loginRequired, userApi.putNakamAvatar);

  app.route('/nakamail')
    .put(authApi.loginRequired, userApi.putUserEmail);

  app.route('/nakapw')
    .put(authApi.loginRequired, userApi.putUserPw);

};
