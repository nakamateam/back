module.exports = function (app) {
  const groupApi = require('../controllers/adminController');
  const authApi = require('../controllers/authController');

  app.route('/dashboard')
    .get(authApi.loginRequiredAndisAdmin, groupApi.getnakamaWayGroups);

  app.route('/total-users')
    .get(authApi.loginRequiredAndisAdmin, groupApi.getNakamaWayTotalUsers);

  app.route('/mongo-stats')
    .get(authApi.loginRequiredAndisAdmin, groupApi.getMongoDbStats);

};
