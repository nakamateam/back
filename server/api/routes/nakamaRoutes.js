module.exports = function (app) {
  const nakamApi = require('../controllers/nakamaController');
  const authApi = require('../controllers/authController');

  app.route('/newnakamas')
    .post(authApi.loginRequired, nakamApi.findNewNakamasAutocomplete);

  app.route('/nakamas')
    .get(authApi.loginRequired, nakamApi.getMyNakamas);

  app.route('/acceptednakamas')
    .get(authApi.loginRequired, nakamApi.getMyAcceptedNakamas);

  app.route('/sendrequest')
    .put(authApi.loginRequired, nakamApi.sendNakamaRequest);

  app.route('/acceptrequest')
    .put(authApi.loginRequired, nakamApi.acceptNakamaRequest);

  app.route('/deletenakama')
    .put(authApi.loginRequired, nakamApi.removeNakama);

  app.route('/cancelrequest')
    .put(authApi.loginRequired, nakamApi.cancelRequest);
};

// authApi.loginRequired,
