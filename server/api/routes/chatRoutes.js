module.exports = function (app) {
  const chatApi = require('../controllers/chatController');
  const authApi = require('../controllers/authController');

  app.route('/chatroom/:room/:from')
    .get(authApi.loginRequired, chatApi.getChatRoomMessages);

  app.route('/chat-links/:room')
    .get(authApi.loginRequired, chatApi.getChatLinks);

  app.route('/chat-user')
    .get(authApi.loginRequired, chatApi.getUser);

  app.route('/chat/unread/:room')
    .get(authApi.loginRequired, chatApi.getUnreadMesage);

  app.route('/chat/clear/unread')
    .put(authApi.loginRequired, chatApi.updateUnreadMessagesInRoom);


};
