const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const messageFile = require('./messageFileModel').schema;
const messageReadStatus = require('./messageReadStatus').schema;



const ChatMessageSchema = new Schema({
  groupId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'Group'
  },

  userId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },

  file: {
    type: [messageFile],
  },

  userAvatar: {
    type: String,
    ref:'User'
  },

  username: {
    type: String,
  },

  message: {
    type: String,
  },

  postedAt: {
    type: Date,
  },

  seenBy: {
    type: [messageReadStatus],
  },

  timestamp: {
    type: String,
  },

  linkMetaData: {
    type: Object,
  }

});

module.exports = mongoose.model('ChatMessage', ChatMessageSchema);
