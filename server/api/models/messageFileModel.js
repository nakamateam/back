const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageFileSchema = new Schema({
  fileId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'File'
  },

  originalname: {
    type: String,
    required: true,
  },

  mimetype: {
    type: String,
  },

  filename: {
    type: String,
    required: true,
  },

  size: {
    type: Number
  },

  postedBy:{
    type: String
  },

});

module.exports = mongoose.model('messageFile', MessageFileSchema);
