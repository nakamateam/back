const ADMIN = {
  value: 'ADMIN'
};

const PARTICIPANT = {
  value: 'PARTICIPANT'
};

const GROUPROLES = [ADMIN.value, PARTICIPANT.value];

module.exports = {
  GROUPROLES, ADMIN, PARTICIPANT
};


