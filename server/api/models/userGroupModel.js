const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const groupRoles = require('./groupRoleEnum');
const notificationPreferences = require('./notificationPreferenceModel').schema;
const notifPref = mongoose.model('NotificationPreference');

const UserGroupSchema = new Schema({
  userId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },

  groupRole: {
    type: String,
    enum: groupRoles.GROUPROLES,
    default: groupRoles.PARTICIPANT.value
  },

  notificationPreferences: {
    type: notificationPreferences,
    default: new notifPref()
  },

  joinDate: {
    type: Date,
  },

  lastConnectionDate: {
    type: Date,
  },

  lastVisit: {
    type: Date,
  },

});

module.exports = mongoose.model('userGroup', UserGroupSchema);
