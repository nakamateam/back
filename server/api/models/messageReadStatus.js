const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageReadStatusSchema = new Schema({
  userId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },

  seenAt: {
    type: Date,
  },

  hasBeenRead: {
    type: Boolean,
    default: false
  },

});

module.exports = mongoose.model('messageRead', MessageReadStatusSchema);
