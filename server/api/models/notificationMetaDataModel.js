const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationMetaDataSchema = new Schema({
  groupId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'Group'
  },

  fileId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'File'
  },

  eventId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'Event'
  },

  isImage: {
    type: Boolean
  },

  filename: {
    type: String,
  },

  oldGroupName: {
    type: String,
  },

  newGroupName: {
    type: String,
  },
});

module.exports = mongoose.model('notificationMetaData', NotificationMetaDataSchema);
