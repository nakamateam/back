const NONE = {
  value: 'NONE'
};

const ONTIME = {
  value: 'ONTIME'
};

const FIVEMIN = {
  value: '5MIN'
};

const FIFTEENMIN = {
  value: '15MIN'
};

const THIRTYMIN = {
  value: '30MIN'
};

const ONEHOUR = {
  value: '1HOUR'
};

const ONEDAY = {
  value: '1DAY'
};

const TWODAYS = {
  value: '2DAYS'
};

const EVENTALERT = [
  ONTIME.value,
  FIVEMIN.value,
  FIFTEENMIN.value,
  THIRTYMIN.value,
  ONEHOUR.value,
  ONEDAY.value,
  TWODAYS.value,
  NONE.value];

module.exports = {
  EVENTALERT, ONTIME, FIVEMIN, FIFTEENMIN, THIRTYMIN, ONEHOUR, ONEDAY, TWODAYS, NONE
};


