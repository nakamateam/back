const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
const bcrypt = require('bcrypt');
const roles = require('./roleEnum');
const notification = require('./notificationModel').schema;
const groupData = require('./groupData').schema;
const friends = require("mongoose-friends");
const pathname = "nakamas";

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    minlength: 2
  },

  firstName: {
    type: String,
    required: true,
    minlength: 2
  },

  lastName: {
    type: String,
    required: true,
    minlength: 2
  },

  email:{
    type: String,
    required: true,
    unique: true,
  },

  hashPassword: {
    type: String,
    require: true
  },

  creationDate: {
    type: Date,
  },

  myGroups:{
    type: [groupData],
  },

  nakamaId:{
    type: Number,
    unique: true,
    minlength: 12
  },

  notifications:{
    type: [notification],
  },

  lastSignInDate: {
    type: Date,
    default: Date.now()
  },

  hasAvatar: {
    type: Boolean,
    default: false
  },

  avatarUrl: {
    type: String,
    default: process.env.DEFAULT_AVATAR_URL
  },

  resetPasswordToken: {
    type: String
  },

  resetPasswordExpires: {
    type: Date
  },

  role: {
    type: String,
    enum: roles.ROLES,
    default: roles.NAKAMA.value
  }
});

UserSchema.plugin(friends({pathName: pathname}));

UserSchema.plugin(autoIncrement.plugin, {
  model: 'User',
  field: 'nakamaId',
  startAt: 505738943947,
  incrementBy: Math.floor(Math.random() * 952637) + 1
});

UserSchema.methods.comparePassword = function (password) {
  return bcrypt.compareSync(password, this.hashPassword);
};

UserSchema.methods.isAdmin = function () {
  return this.role === roles.NAKAMADMIN.value;
};

module.exports = mongoose.model('User', UserSchema);
