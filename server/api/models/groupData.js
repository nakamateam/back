const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const groupRoles = require('./groupRoleEnum');

const groupDataSchema = new Schema({
  groupId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'Group'
  },

  groupRole: {
    type: String,
    enum: groupRoles.GROUPROLES,
    default: groupRoles.PARTICIPANT.value
  }

});

module.exports = mongoose.model('groupData', groupDataSchema);
