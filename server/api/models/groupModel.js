const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const groupRole = require('./groupRoleEnum');
const userGroup = require('./userGroupModel').schema;
const userCreator = mongoose.model('userGroup');

const GroupSchema = new Schema({
  creatorId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },

  groupName: {
    type: String,
    required: true,
    minlength: 1,
    maxlength:50
  },

  users: {
    type: [userGroup],
  },

  creationDate: {
    type: Date,
  },

  hasAvatar: {
    type: Boolean,
    default: false
  },

  avatarUrl: {
    type: String,
    default: process.env.DEFAULT_AVATAR_URL
  },

  groupSize: {
    type: Number,
    default: 0
  }

});

GroupSchema.pre('save', function(next) {
  const groupCreator = new userCreator();
  groupCreator.userId = this.creatorId;
  groupCreator.groupRole = groupRole.ADMIN.value;
  groupCreator.joinDate = this.creationDate;
  this.users.push(groupCreator);
  next();
});

module.exports = mongoose.model('Group', GroupSchema);
