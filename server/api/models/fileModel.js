const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FileSchema = new Schema({
  fieldname: {
    type: String,
    required: true,
  },

  originalname:{
    type: String,
    required: true,
  },

  groupId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'Group'
  },

  postedBy:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },

  encoding: {
    type: String,
  },

  mimetype: {
    type: String
  },

  destination: {
    type: String,
  },

  filename: {
    type: String,
    required: true,
    unique: true,
  },

  path: {
    type: String,
    required: true,
  },

  size: {
    type: Number
  },

  creationDate: {
    type: Date,
    default: Date.now()
  },
});


module.exports = mongoose.model('File', FileSchema);
