const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('./userModel');
const notifTypes = require('./notificationEnum');
const notificationMetaData = require('./notificationMetaDataModel').schema;

const NotificationSchema = new Schema({
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },

  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },

  created_at: {
    type: Date,
  },

  unRead: {
    type: Boolean,
    default: true
  },

  notificationType: {
    type: String,
    enum: notifTypes.NOTIFTYPES,
  },

  metaData: {
    type: notificationMetaData,
  }
});

module.exports = mongoose.model('Notification', NotificationSchema);
