const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EventColorSchema = new Schema({

  primary: {
    type: String,
    default: '#3F51B5'
  },

  secondary:{
    type: String,
    default: '#b3e1f7'
  }
});

module.exports = mongoose.model('EventColor', EventColorSchema);
