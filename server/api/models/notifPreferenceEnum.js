const ALLNOTIFOFF = {
  value: 'allNotifOff'
};

const CHAT = {
  value: 'chat'
};

const CALENDAR = {
  value: 'calendar'
};

const SHAREDFILES = {
  value: 'sharedFiles'
};

const SETTINGS = {
  value: 'settings'
};



const NOTIFPREFTYPES = [ALLNOTIFOFF.value, CHAT.value, CALENDAR.value, SHAREDFILES.value, SETTINGS.value];

module.exports = {
  NOTIFPREFTYPES, ALLNOTIFOFF, CHAT, CALENDAR, SHAREDFILES, SETTINGS
};


