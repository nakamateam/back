const NAKAMADMIN = {
  value: 'NAKAMADMIN'
};

const NAKAMA = {
  value: 'NAKAMA'
};



const ROLES = [NAKAMADMIN.value, NAKAMA.value];

module.exports = {
  ROLES, NAKAMADMIN, NAKAMA
};


