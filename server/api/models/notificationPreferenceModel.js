const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationPreferenceSchema = new Schema({

  allNotifOff: {
    type: Boolean,
    default: false
  },

  chat: {
    type: Boolean,
    default: true
  },

  calendar: {
    type: Boolean,
    default: true
  },

  sharedFiles: {
    type: Boolean,
    default: true
  },

  settings: {
    type: Boolean,
    default: true
  },
});

module.exports = mongoose.model('NotificationPreference', NotificationPreferenceSchema);
