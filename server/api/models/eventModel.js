const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const eventAlert = require('./eventAlertEnum');
const colorEvent = require('./eventColorModel').schema;
const colorDefault = mongoose.model('EventColor');

const eventSchema = new Schema({

  start:{
    type: Date
  },

  end:{
    type: Date
  },

  title:{
    type: String
  },

  description:{
    type: String
  },

  place:{
    type: String
  },

  alert:{
    type: String,
    enum: eventAlert.EVENTALERT,
    default: eventAlert.NONE.value
  },

  color: colorEvent,

  draggable: {
    type: Boolean,
    default: true
  },

  creationDate: {
    type: Date,
  },

  eventCreatorId: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },

  groupId: {
    type: mongoose.Schema.Types.ObjectId,
    ref:'Group',
    required: true
  },
});

eventSchema.pre('save', function(next) {
  this.color = new colorDefault();
  next();
});

module.exports = mongoose.model('Event', eventSchema);
