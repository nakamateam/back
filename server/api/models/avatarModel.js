const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AvatarSchema = new Schema({
  fieldname: {
    type: String,
    required: true,
  },

  originalname:{
    type: String,
    required: true,
  },

  encoding: {
    type: String,
  },

  mimetype: {
    type: String
  },

  destination: {
    type: String,
  },

  filename: {
    type: String,
    required: true,
    unique: true,
  },

  path: {
    type: String,
    required: true,
  },

  size: {
    type: Number
  },

  creationDate: {
    type: Date,
    default: Date.now()
  },
});


module.exports = mongoose.model('Avatar', AvatarSchema);
