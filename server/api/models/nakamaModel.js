const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const statusTypes = require('./nakamaStatusEnum');

const NakamaSchema = new Schema({
  nakamaId:{
    type: Number,
    minlength: 12
  },

  statusType: {
    type: String,
    enum: statusTypes.STATUSTYPES,
  }

});

module.exports = mongoose.model('Nakama', NakamaSchema);
