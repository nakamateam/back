const socketioJwt = require('socketio-jwt');
const nakamaHelper = require('../helpers/getMyNakamasIds');
const mongoose = require('mongoose');
const ChatMessage = mongoose.model('ChatMessage');
const ChatMessageReadStatus = mongoose.model('messageRead');
const notifEnum = require('../api/models/notificationEnum');
const uuidv1 = require('uuid/v1');
const ogs = require('open-graph-scraper');
const Group = mongoose.model('Group');

class NotificationSocket{

  constructor(){
    //Array to store the list of users along with there respective socket id.
    this.users = [];
  }

  updateMessageStatus(message, io) {
    const NotificationSocket = this;
    Group.findById(message.groupId, {users: 1, groupName: 1, avatarUrl: 1}, function (err, group) {
      if (err) {
        console.log(err);
      } else {
        if (group) {
          let groupMessageStatusArray = [];
          group.users.forEach(user => {
            groupMessageStatusArray.push(new ChatMessageReadStatus({userId: user.userId}));
          });
          if (io.sockets.adapter.rooms[message.groupId]) {
            console.log(io.sockets.adapter.rooms[message.groupId].sockets);
            let usersSocketIds = Object.keys(io.sockets.adapter.rooms[message.groupId].sockets);
            activeSocketUsers.forEach(function (activeUser) {
              usersSocketIds.forEach(function (socketId) {
                if (activeUser.id === socketId) {
                  // update groupMessageStatusArray status
                  let userIndex = groupMessageStatusArray.findIndex(user => user.userId.toString() === activeUser.userId);
                  if (userIndex !== -1) {
                    groupMessageStatusArray[userIndex].hasBeenRead = true;
                    groupMessageStatusArray[userIndex].seenAt = new Date();
                  }
                }
              });
            });
            ChatMessage.findOneAndUpdate({_id: message._id}, {$set: {seenBy: groupMessageStatusArray}}, {new: true}, function (err, message) {
              if (err) {
                console.log(err);
              } else {
                if (message) {
                  //let non connected to the way member know they have a new message if they are connected
                  message.seenBy.forEach(user => {
                    // find user preferences
                    let userPreferences = group.users.find(function (userPref) {
                      return userPref.userId.toString() === user.userId.toString();
                    });
                    if (!user.hasBeenRead && userPreferences && userPreferences.notificationPreferences.chat) {
                      let newMessageNotification = {
                        senderInfo: {
                          username : message.username,
                          avatarUrl : message.userAvatar,
                        },
                        created_at: message.postedAt,
                        notificationType: notifEnum.NEWMESSAGE.value,
                        groupInfo: {
                          groupName: group.groupName,
                          avatarUrl: group.avatarUrl
                        },
                        unRead: true,
                        metaData: {
                          groupId: message.groupId
                        }
                      };
                      NotificationSocket.sendLiveNotification(io, user.userId.toString(), newMessageNotification);
                    }
                  });
                }
              }
            });
          } else {
            ChatMessage.findOneAndUpdate({_id: message._id}, {$set: {seenBy: groupMessageStatusArray}}, {new: true}, function (err, message) {
              if (err) {
                console.log(err);
              }
            });
          }
        }
      }
    });
  }

  SendCleanActiveSocketUser(io) {
    let activeUsers = JSON.parse(JSON.stringify(activeSocketUsers));
    let itemProcessed = 0;
    activeUsers.forEach(function (activeUser) {
      itemProcessed++;
      delete activeUser.id;
      if (itemProcessed === activeUsers.length) {
        io.emit('activeSocketUsers', activeUsers);
      }
    });
  }

  letMyNakamaKnowIamConnected(io, newConnectedUserId) {
    // Find all Friend of new connected user
    nakamaHelper.findMyNakamasIds(newConnectedUserId)
      .then(function(nakamas) {
        if (nakamas.length > 0) {
          //inform all nakamas of the new connected user that he is now connected
          nakamas.forEach(function (nakama) {
            activeSocketUsers.forEach(function (activeUser) {
              if (nakama._id.toString() === activeUser.userId) {
                io.to(activeUser.id).emit('NakamaConnection', newConnectedUserId);
              }
            })
          })
        }
    }, function(error) {
      console.log(error);
    });
  }

  updateMyNakamasRoomConnectedUsers(io, connectedSocketIds, roomId) {
    const connectedInRoomUserIds = [];
    activeSocketUsers.forEach(function (activeUser) {
      connectedSocketIds.forEach(function (socketId) {
        if (activeUser.id === socketId) {
          connectedInRoomUserIds.push(activeUser.userId);
        }
      });
    });
    nakamaHelper.getConnectedUserData(connectedInRoomUserIds)
      .then(function (connectedUsers) {
        io.in(roomId).emit('room users', connectedUsers);
      }, function(error) {
      console.log(error);
    });
  }

  getRoomConnectedUsers(io, connectedSocketIds, mySocketId, roomId) {
    const connectedInRoomUserIds = [];
    activeSocketUsers.forEach(function (activeUser) {
      connectedSocketIds.forEach(function (socketId) {
        if (activeUser.id === socketId) {
          connectedInRoomUserIds.push(activeUser.userId);
        }
      });
    });
    nakamaHelper.getConnectedUserData(connectedInRoomUserIds)
      .then(function (connectedUsers) {
        io.to(mySocketId).emit(`${roomId} connected users`, connectedUsers);
      }, function(error) {
        console.log(error);
      });
  }

  updateRoomConnectedUsers(io, connectedSocketIds, roomId, socket) {
    const connectedInRoomUserIds = [];


    activeSocketUsers.forEach(function (activeUser, i) {
      connectedSocketIds.forEach(function (socketId) {
        if (activeUser.id === socketId) {
          connectedInRoomUserIds.push(activeUser.userId);
        }
        if (i === activeSocketUsers.length - 1) {
          nakamaHelper.getConnectedUserData(connectedInRoomUserIds)
            .then(function (connectedUsers) {
              socket.broadcast.emit(`${roomId} connected users`, connectedUsers);
            }, function(error) {
              console.log(error);
            });
        }
      });
  });
    if (connectedSocketIds.length === 0) {
      socket.broadcast.emit(`${roomId} connected users`, connectedSocketIds);
    }
  }

  letMyNakamaKnowILeft(io, newConnectedUserId) {
    // Find all Friend of new connected user
    nakamaHelper.findMyNakamasIds(newConnectedUserId)
      .then(function(nakamas) {
        if (nakamas.length > 0) {
          //inform all nakamas of the new connected user that he is now connected
          nakamas.forEach(function (nakama) {
            activeSocketUsers.forEach(function (activeUser) {
              if (nakama._id.toString() === activeUser.userId) {
                io.to(activeUser.id).emit('NakamaDisonnection', newConnectedUserId);
              }
            })
          })
        }
      }, function(error) {
        console.log(error);
      });
  }

  getMyActiveNakama(io, myId, mySocketId) {
    // Find all Friend of new connected user
    let myConnectedNakama = [];
    let itemProcessed = 0;
    nakamaHelper.findMyNakamasIds(myId)
      .then(function(nakamas) {
        if (nakamas.length > 0) {
          //inform all nakamas of the new connected user that he is now connected
          nakamas.forEach(function (nakama) {
            itemProcessed++;
            let activeUserProcessed = 0;
            activeSocketUsers.forEach(function (activeUser) {
              activeUserProcessed ++;
              if (nakama._id.toString() === activeUser.userId) {
                myConnectedNakama.push(activeUser.userId);
              }
              if (itemProcessed === nakamas.length && activeUserProcessed === activeSocketUsers.length) {
                io.to(mySocketId).emit('myConnectedNakamas', myConnectedNakama);
              }
            })
          })
        }
      }, function(error) {
        console.log(error);
      });
  }

  handleChatMessageLinkMeta(message, io) {
    let options = {'url': message.link.href, 'timeout': 4000};
    ogs(options, function (error, results) {
      console.log('error:', error); // This is returns true or false. True if there was a error. The error it self is inside the results object.
      if (error === false) {
        message.linkMetaData = results.data;
        //Send immediatly to socket room the metaData from ogs and update the message using the generated timestamp
        io.in(message.groupId).emit('update message meta', message);
        ChatMessage.findOneAndUpdate({timestamp: message.timestamp}, {$set: {linkMetaData: results.data}}, {new: true}, function (err, group) {
          if (err) {
            console.log(err);
          }
        });
      }
      console.log('results:', results);
    });
  }

  updateActiveSocketUsersList(io){
    const NotificationSocket = this;
    io
      .on('connection', socketioJwt.authorize({
        secret: process.env.SECRET_KEY.trim()
      })).on('authenticated', function(socket) {
      //this socket is authenticated, we are good to handle more events from it.
      let currentRoomId;
      socket.on('refreshList', () => {
        NotificationSocket.getMyActiveNakama(io, socket.decoded_token._id, socket.id);
      });

      // 'get connected users by group'
      socket.on('toctocroom', (groupId) => {
        if (io.sockets.adapter.rooms[groupId]) {
          let usersSocketIds = Object.keys(io.sockets.adapter.rooms[groupId].sockets);
          NotificationSocket.getRoomConnectedUsers(io, usersSocketIds, socket.id, groupId);
        }
      });

      activeSocketUsers.push({
        id : socket.id,
        userId : socket.decoded_token._id
      });
      NotificationSocket.letMyNakamaKnowIamConnected(io, socket.decoded_token._id);


      // 'join event'
      socket.on('join', (data) => {
        socket.join(data.groupId);
        currentRoomId = data.groupId;
        if (io.sockets.adapter.rooms[data.groupId]) {
          let usersSocketIds = Object.keys(io.sockets.adapter.rooms[data.groupId].sockets);
          NotificationSocket.updateMyNakamasRoomConnectedUsers(io, usersSocketIds, data.groupId);
          NotificationSocket.updateRoomConnectedUsers(io, usersSocketIds, data.groupId, socket);
        }
      });
      // catching the message event
      socket.on('message', (message) => {
        let timestamp = uuidv1();
        message.timestamp = timestamp;
        io.in(message.groupId).emit('new message', message);
        if (message.link) {
          NotificationSocket.handleChatMessageLinkMeta(message, io);
        }
        // emitting the 'new message' event to the clients in that room
        // save the message in the 'messages' array of that chat-room
        const newChatMessage = new ChatMessage(message);
        newChatMessage.groupId = message.groupId;
        newChatMessage.userId = socket.decoded_token._id;
        newChatMessage.timestamp = timestamp;
        newChatMessage.save( (err, newMessage) => {
          if(err) {
            console.log(err);
          } else {
            NotificationSocket.updateMessageStatus(newMessage, io);
          }
        });
      });
      // Event when a client is typing
      socket.on('typing', (data) => {
        // Broadcasting to all the users except the one typing
        socket.broadcast.in(data.groupId).emit('typing', {username: data.username, avatarUrl: data.avatarUrl, hasAvatar: data.hasAvatar, isTyping: true});
      });
      // Event when a client stop typing
      socket.on('stop typing', (data) => {
        // Broadcasting to all the users except the one typing
        socket.broadcast.in(data.groupId).emit('stop typing', {username: data.username, isTyping: false});
      });

      // 'leave room event'
      socket.on('leave', (data) => {
        socket.leave(data.groupId);
        if (io.sockets.adapter.rooms[data.groupId]) {
        let usersSocketIds = Object.keys(io.sockets.adapter.rooms[data.groupId].sockets);
        NotificationSocket.updateMyNakamasRoomConnectedUsers(io, usersSocketIds, data.groupId);
        NotificationSocket.updateRoomConnectedUsers(io, usersSocketIds, data.groupId, socket);
        } else {
          NotificationSocket.updateRoomConnectedUsers(io, [], data.groupId, socket);
        }
      });

      socket.on('disconnect',()=>{
        //let room user know if user left without leave room event (closing browser)
        if (currentRoomId) {
          if (io.sockets.adapter.rooms[currentRoomId]) {
            let usersInRoomSocketIds = Object.keys(io.sockets.adapter.rooms[currentRoomId].sockets);
            NotificationSocket.updateMyNakamasRoomConnectedUsers(io, usersInRoomSocketIds, currentRoomId);
            NotificationSocket.updateRoomConnectedUsers(io, usersInRoomSocketIds, currentRoomId, socket);
          }
        }

        for(let i=0; i < activeSocketUsers.length; i++){

          if(activeSocketUsers[i].id === socket.id){
            activeSocketUsers.splice(i,1);
          }
        }
        NotificationSocket.letMyNakamaKnowILeft(io, socket.decoded_token._id);
        NotificationSocket.SendCleanActiveSocketUser(io);
      });

    });
  }

  sendLiveNotification(socketIo, receiver, notif) {
    activeSocketUsers.forEach(function (activeUser) {
      if(activeUser.userId === receiver) {
        socketIo.to(activeUser.id).emit('newNotif', notif);
      }
    });
  }

  sendLiveAlertNotification(socketIo, receiver, notif) {
    activeSocketUsers.forEach(function (activeUser) {
      if(activeUser.userId === receiver) {
        socketIo.to(activeUser.id).emit('newEventAlertNotif', notif);
      }
    });
  }

  updateLiveNotification(socketIo, nakamaId, notif) {
  activeSocketUsers.forEach(function (activeUser) {
    if(activeUser.userId === nakamaId) {
      socketIo.to(activeUser.id).emit('newNotif', notif);
    }
  });
}

  letMyNewNakamaKnowIAmConnected(socketIo, myNewNakamaId, myId) {
    activeSocketUsers.forEach(function (activeUser) {
      if(activeUser.userId === myNewNakamaId) {
        socketIo.to(activeUser.id).emit('NakamaConnection', myId);
      }
    });
  }
}
module.exports = NotificationSocket;
