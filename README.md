# NakamaWay

NakamaWay is a social Network made for true friends. A simple interface given the possibility for users to 
interact, share events and files through group called "ways".

This is the back-end of the application created with node.js.

Before launching the project for the first time, you need to create a "public folder" at the root of the project.

=> Inside this folder add 3 others folders: "avatar" / "files" / "way"

=> Inside "way" add the folder "avatar"

These folders are used to store the application folder in their respective location using the library multer.
However "multer" will not creates the folder for you and might generate an error.

## Database
Before launching the project for the first time, you need to create a "nakamaWayDB" folder at the root of the project.
This folder will be used when using the application in local mode.
The database is "MongoDb" and interacts with the node server through the "mongoose" library.
The database will be available on `http://localhost:27017`


## Development server (local env)

Run `npm run startServer` for a dev server. Navigate to `http://localhost:300/`. The app will automatically reload if you change any of the source files.

