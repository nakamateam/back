const express = require('express');
const cors = require('cors');
const Cookies = require('cookies');
const morgan = require('morgan');
require('dotenv').config({ path: `${__dirname}/server/config/${process.env.NODE_ENV}/.env`});
console.log('Server environment : ' + process.env.NODE_ENV);

const app = express();
let http;
let server;
if (process.env.NODE_ENV === 'local') {
  http = require('http');
  server = http.createServer(app);
} else {
  http = require('https');
  const fs = require('fs');
  const options = {
    key: fs.readFileSync(process.env.HTTPS_PRIVATE_KEY),
    cert: fs.readFileSync(process.env.HTTPS_CERT),
    ca: fs.readFileSync(process.env.HTTPS_CA)
  };
  server = http.createServer(options, app);
}


const io = require('socket.io').listen(server);
io.set('origins', '*:*');
const socketIOHelper = require('./server/helpers/socketio');
server.listen(process.env.PORT);
socketIOHelper.set(io);
const mongoose = require('mongoose');
const jsonwebtoken = require('jsonwebtoken');

app.proxy = true;
app.set('trust proxy', 1);

const bodyParser = require('body-parser');

const corsOptions = {
  origin: process.env.CORS_ORIGIN,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  optionsSuccessStatus: 200,
  allowedHeaders : 'Content-Type,Authorization, x-xsrf-token, ioToken, way-id',
  exposedHeaders : 'Content-Range,X-Content-Range, Accept-Ranges, Content-Encoding, Content-Length, Content-Range'
};

app.use(cors(corsOptions));

// interceptor : verify the authorization
app.use(function(req, res, next) {
  // retrieve XSRF token from request header
  const xsrfToken = req.headers['x-xsrf-token'];

  // retrieve JWT token from the cookie
  const token = new Cookies(req, res).get('nakamaToken');

  // proceed JWT Token check
  if ( xsrfToken && token) {
    jsonwebtoken.verify(token, process.env.SECRET_KEY, function(err, decoded) {
      if (err) {
        req.user = undefined;
        next();
      }
      if (decoded.xsrfToken !== xsrfToken) {
        // error CSRF attack! so the user is not connected
        req.user = undefined;
        next();
      } else {
        req.user = decoded;
        next();
      }
    });
  } else {
    req.user = undefined;
    next();
  }
});

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
if (process.env.NODE_ENV === 'local') {
  console.log('local connection to nakamaDB');
  mongoose.connect(`mongodb://${process.env.DB_HOST}/${process.env.DB_DATABASE}`, { useNewUrlParser: true })
      .then(()=>{
        console.log('MongoDB is connected')
      }).catch(err=>{
    console.log('MongoDB connection unsuccessful.');
    console.log(err);
  });
} else {
  console.log('remote connection to nakamaDB');
  mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:27017/${process.env.DB_DATABASE}`, { useNewUrlParser: true })
      .then(()=>{
        console.log('MongoDB is connected')
      }).catch(err=>{
    console.log('MongoDB connection unsuccessful.')
  });
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use('/public/avatar', express.static(__dirname + '/public/avatar'));
app.use('/public/way/avatar', express.static(__dirname + '/public/way/avatar'));

//importing _models
const User = require('./server/api/models/userModel');
const Avatar = require('./server/api/models/avatarModel');
const Group = require('./server/api/models/groupModel');
const groupData = require('./server/api/models/groupData');
const file = require('./server/api/models/fileModel');
const Event = require('./server/api/models/eventModel');
const ChatMessage = require('./server/api/models/chatMessageModel');
const messageRead = require('./server/api/models/messageReadStatus');


//creating global var
global.activeSocketUsers = [];
//creating sockets
const notifSocket = require('./server/sockets/notifSocket');
new notifSocket().updateActiveSocketUsersList(io);

const eventSheduleReset = require('./server/api/repository/eventRepository');
eventSheduleReset.resetEventAlert();


// importing routes
const userRoutes = require('./server/api/routes/userRoutes');
userRoutes(app);
const authRoutes = require('./server/api/routes/authRoutes');
authRoutes(app);
const avatarRoutes = require('./server/api/routes/avatarRoutes');
avatarRoutes(app);
const nakamaRoutes = require('./server/api/routes/nakamaRoutes');
nakamaRoutes(app);
const notificationRoutes = require('./server/api/routes/notificationRoutes');
notificationRoutes(app);
const groupRoutes = require('./server/api/routes/groupRoutes');
groupRoutes(app);
const fileRoutes = require('./server/api/routes/fileRoutes');
fileRoutes(app);
const eventRoutes = require('./server/api/routes/eventRoutes');
eventRoutes(app);
const chatRoutes = require('./server/api/routes/chatRoutes');
chatRoutes(app);
const adminRoutes = require('./server/api/routes/adminRoutes');
adminRoutes(app);

app.get('/', (req, res) => res.send('Do you fell more as a Nakama or Okama?'));

console.log('NakamaWay\'s API server started on: ' + process.env.PORT);
